package sample.Presentation;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * Created by Roxana on 08-Oct-16.
 */
public class AddItemsScene {
    private static Label title, itemName, itemPrice, itemMaxCapacity;
    private static TextField nameField, priceField, maxCapacityField;
    private static Button addItemButton, backButton;
    private static BorderPane borderPane;
    private static VBox itemLabels, itemTextFields;
    private static HBox labelsAndTfieldsTogether, buttonsTogether;
    private static Scene scene;

    public static Scene initialize() {

        //title
        title = new Label("Add New Item");
        title.setId("individualLabel");


        itemName = new Label("Name: ");
        nameField = new TextField();
        nameField.setPromptText("add item here");
        nameField.setFocusTraversable(false);

        itemPrice = new Label("Price: ");
        priceField = new TextField();
        priceField.setPromptText("enter price here");
        priceField.setFocusTraversable(false);

        itemMaxCapacity = new Label("Max capacity: ");
        maxCapacityField = new TextField();
        maxCapacityField.setPromptText("add max cap. here");
        maxCapacityField.setFocusTraversable(false);

        addItemButton = new Button("Add");
        backButton = new Button("Back");

        itemLabels = new VBox(25);
        itemLabels.getChildren().addAll(itemName, itemPrice, itemMaxCapacity);

        itemTextFields = new VBox(13);
        itemTextFields.getChildren().addAll(nameField, priceField, maxCapacityField);

        labelsAndTfieldsTogether = new HBox(20);
        labelsAndTfieldsTogether.getChildren().addAll(itemLabels, itemTextFields);

        buttonsTogether = new HBox(20);
        buttonsTogether.getChildren().addAll(backButton, addItemButton);

        borderPane = new BorderPane();
        borderPane.setPadding(new Insets(20));
        borderPane.setAlignment(title, Pos.CENTER);
        borderPane.setTop(title);
        borderPane.setCenter(labelsAndTfieldsTogether);
        labelsAndTfieldsTogether.setPadding(new Insets(20));
        borderPane.setBottom(buttonsTogether);

        scene = new Scene(borderPane, 1600, 600);
        scene.getStylesheets().add("style.css");
        return scene;
    }

    public static Label getTitle() {
        return title;
    }

    public static HBox getLabelsAndTfieldsTogether() {
        return labelsAndTfieldsTogether;
    }

    public static Label getItemName() {
        return itemName;
    }

    public static Label getItemPrice() {
        return itemPrice;
    }

    public static Label getItemMaxCapacity() {
        return itemMaxCapacity;
    }

    public static TextField getNameField() {
        return nameField;
    }

    public static TextField getPriceField() {
        return priceField;
    }

    public static TextField getMaxCapacityField() {
        return maxCapacityField;
    }

    public static Button getAddItemButton() {
        return addItemButton;
    }

    public static Button getBackButton() {
        return backButton;
    }

    public static BorderPane getBorderPane() {
        return borderPane;
    }

    public static VBox getItemLabels() {
        return itemLabels;
    }

    public static VBox getItemTextFields() {
        return itemTextFields;
    }

    public static HBox getButtonsTogether() {
        return buttonsTogether;
    }

    public static Scene getScene() {
        return scene;
    }
}
