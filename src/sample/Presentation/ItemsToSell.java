package sample.Presentation;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * Created by Roxana on 08-Oct-16.
 */
public class ItemsToSell {
    private static ListView<String> listView;
    private static Button addButton, editButton, backButton;
    private static HBox hBoxButtonBarActivities;
    private static Label headerActivitiesLabel;
    private static BorderPane pane;
    private static Scene scene;

    public static Scene initialize() {
        listView = new ListView<String>();

        //listView.setItems();

        listView = new ListView();
        listView.setEditable(true);
        addButton = new Button("Add");
        editButton = new Button("Edit");
        backButton = new Button("Back");

        hBoxButtonBarActivities = new HBox(40);
        hBoxButtonBarActivities.setAlignment(Pos.CENTER);
        hBoxButtonBarActivities.getChildren().addAll(backButton, addButton, editButton);

        headerActivitiesLabel = new Label("Items to sell");
        headerActivitiesLabel.setId("individualLabel");

        VBox vBox = new VBox(30);
        vBox.getChildren().addAll(headerActivitiesLabel, listView);
        pane = new BorderPane();
        pane.setAlignment(headerActivitiesLabel, Pos.CENTER);
        pane.setTop(headerActivitiesLabel);
        pane.setCenter(listView);
        pane.setAlignment(hBoxButtonBarActivities, Pos.CENTER);
        pane.setBottom(hBoxButtonBarActivities);

        scene = new Scene(pane, 1600, 600);
        scene.getStylesheets().add("style.css");
        return scene;

    }

    public static ListView<String> getListView() {
        return listView;
    }

    public static Button getAddButton() {
        return addButton;
    }

    public static Button getEditButton() {
        return editButton;
    }

    public static Button getBackButton() {
        return backButton;
    }

    public static HBox gethBoxButtonBarActivities() {
        return hBoxButtonBarActivities;
    }

    public static Label getHeaderActivitiesLabel() {
        return headerActivitiesLabel;
    }

    public static BorderPane getPane() {
        return pane;
    }

    public static Scene getScene() {
        return scene;
    }
}
