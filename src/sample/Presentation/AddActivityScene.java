package sample.Presentation;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class AddActivityScene {
    private static Label title, activityName, activityPrice, activityMaxCapacity;
    private static TextField activityField, priceField, maxCapacityField;
    private static Button addActivityButton, backButton;
    private static BorderPane borderPane;
    private static VBox activityLabels, activityTextFields;
    private static HBox labelsAndTfieldsTogether, buttonsTogether;
    private static Scene scene;

    public static Scene initialize() {

        //title
        title = new Label("Add New Activity");
        title.setId("individualLabel");


        activityName = new Label("Name: ");
        activityField = new TextField();
        activityField.setPromptText("add activity here");
        activityField.setFocusTraversable(false);

        activityPrice = new Label("Price: ");
        priceField = new TextField();
        priceField.setPromptText("enter price here");
        priceField.setFocusTraversable(false);

        activityMaxCapacity = new Label("Max capacity: ");
        maxCapacityField = new TextField();
        maxCapacityField.setPromptText("add max cap. here");
        maxCapacityField.setFocusTraversable(false);

        addActivityButton = new Button("Add");
        backButton = new Button("Back");

        activityLabels = new VBox(25);
        activityLabels.getChildren().addAll(activityName, activityPrice, activityMaxCapacity);

        activityTextFields = new VBox(13);
        activityTextFields.getChildren().addAll(activityField, priceField, maxCapacityField);

        labelsAndTfieldsTogether = new HBox(20);
        labelsAndTfieldsTogether.getChildren().addAll(activityLabels, activityTextFields);

        buttonsTogether = new HBox(20);
        buttonsTogether.getChildren().addAll(backButton, addActivityButton);

        borderPane = new BorderPane();
        borderPane.setPadding(new Insets(20));
        borderPane.setAlignment(title, Pos.CENTER);
        borderPane.setTop(title);
        borderPane.setCenter(labelsAndTfieldsTogether);
        labelsAndTfieldsTogether.setPadding(new Insets(20));
        borderPane.setBottom(buttonsTogether);

        scene = new Scene(borderPane, 1600, 600);
        scene.getStylesheets().add("style.css");
        return scene;
    }

    public static Label getTitle() {
        return title;
    }

    public static void setTitle(Label title) {
        AddActivityScene.title = title;
    }

    public static Label getActivityName() {
        return activityName;
    }

    public static void setActivityName(Label activityName) {
        AddActivityScene.activityName = activityName;
    }

    public static Label getActivityPrice() {
        return activityPrice;
    }

    public static void setActivityPrice(Label activityPrice) {
        AddActivityScene.activityPrice = activityPrice;
    }

    public static Label getActivityMaxCapacity() {
        return activityMaxCapacity;
    }

    public static void setActivityMaxCapacity(Label activityMaxCapacity) {
        AddActivityScene.activityMaxCapacity = activityMaxCapacity;
    }

    public static TextField getActivityField() {
        return activityField;
    }

    public static void setActivityField(TextField activityField) {
        AddActivityScene.activityField = activityField;
    }

    public static TextField getPriceField() {
        return priceField;
    }

    public static void setPriceField(TextField priceField) {
        AddActivityScene.priceField = priceField;
    }

    public static TextField getMaxCapacityField() {
        return maxCapacityField;
    }

    public static void setMaxCapacityField(TextField maxCapacityField) {
        AddActivityScene.maxCapacityField = maxCapacityField;
    }

    public static Button getAddActivityButton() {
        return addActivityButton;
    }

    public static void setAddActivityButton(Button addActivityButton) {
        AddActivityScene.addActivityButton = addActivityButton;
    }

    public static Button getBackButton() {
        return backButton;
    }

    public static void setBackButton(Button backButton) {
        AddActivityScene.backButton = backButton;
    }

    public static BorderPane getBorderPane() {
        return borderPane;
    }

    public static void setBorderPane(BorderPane borderPane) {
        AddActivityScene.borderPane = borderPane;
    }

    public static VBox getActivityLabels() {
        return activityLabels;
    }

    public static void setActivityLabels(VBox activityLabels) {
        AddActivityScene.activityLabels = activityLabels;
    }

    public static VBox getActivityTextFields() {
        return activityTextFields;
    }

    public static void setActivityTextFields(VBox activityTextFields) {
        AddActivityScene.activityTextFields = activityTextFields;
    }

    public static HBox getLabelsAndTfieldsTogether() {
        return labelsAndTfieldsTogether;
    }

    public static void setLabelsAndTfieldsTogether(HBox labelsAndTfieldsTogether) {
        AddActivityScene.labelsAndTfieldsTogether = labelsAndTfieldsTogether;
    }

    public static HBox getButtonsTogether() {
        return buttonsTogether;
    }

    public static void setButtonsTogether(HBox buttonsTogether) {
        AddActivityScene.buttonsTogether = buttonsTogether;
    }

    public static Scene getScene() {
        return scene;
    }

    public static void setScene(Scene scene) {
        AddActivityScene.scene = scene;
    }
}
