package sample.Presentation;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class CompanyScene {
    public static Scene scene;
    public static TextField companyNameTextField, ageOfYoungestTextField, phoneNrTextField, noOfPeopleTextField;
    public static Label companyHeaderLabel, companyNameLabel, ageOfYoungesLabel, phoneNrLabel, noOfPeopleLabel;
    public static HBox hBox, buttonHbox;
    public static VBox labelVbox, textVbox;
    public static BorderPane borderPane;
    public static Button nextButton, backButton;

    public static Scene initialize() {

        //setting the labels
        companyHeaderLabel = new Label("Company booking");
        companyHeaderLabel.setId("individualLabel");
        companyHeaderLabel.setPadding(new Insets(-10,0,20,0));

        companyNameLabel = new Label("Company name: ");
        ageOfYoungesLabel = new Label("Age Of Youngest: ");
        phoneNrLabel = new Label("Phone: ");
        noOfPeopleLabel = new Label("Number of people: ");

        //setting the Text Fields
        companyNameTextField = new TextField();
        companyNameTextField.setPromptText("Enter company name");

        ageOfYoungestTextField = new TextField();
        ageOfYoungestTextField.setPromptText("Enter age");

        phoneNrTextField = new TextField();
        phoneNrTextField.setPromptText("Enter phone");


        noOfPeopleTextField = new TextField();
        noOfPeopleTextField.setPromptText("Enter number of people");

        //setting the buttons
        backButton = new Button("Back");
        nextButton = new Button("Next");

        //setting the HBox
        buttonHbox = new HBox(20);
        buttonHbox.setAlignment(Pos.BOTTOM_RIGHT);
        buttonHbox.getChildren().addAll(backButton, nextButton);

        //setting the VBox
        labelVbox = new VBox(24);
        labelVbox.getChildren().addAll(companyNameLabel,noOfPeopleLabel, ageOfYoungesLabel, phoneNrLabel);


        textVbox = new VBox(14);
        textVbox.getChildren().addAll(companyNameTextField, noOfPeopleTextField, ageOfYoungestTextField, phoneNrTextField);

        hBox = new HBox(15);
        hBox.getChildren().addAll(labelVbox, textVbox);


        //setting the borderpane
        borderPane = new BorderPane();

        borderPane.setPadding(new Insets(20));
        borderPane.setAlignment(companyHeaderLabel, Pos.CENTER);
        borderPane.setTop(companyHeaderLabel);
        borderPane.setCenter(hBox);
        borderPane.setBottom(buttonHbox);

        scene = new Scene(borderPane, 1600, 600);
        scene.getStylesheets().add("style.css");

        return scene;
    }

    public static Scene getScene() {
        return scene;
    }

    public static TextField getCompanyNameTextField() {
        return companyNameTextField;
    }

    public static TextField getAgeOfYoungestTextField() {
        return ageOfYoungestTextField;
    }

    public static TextField getPhoneNrTextField() {
        return phoneNrTextField;
    }

    public static TextField getNoOfPeopleTextField() {
        return noOfPeopleTextField;
    }

    public static Label getCompanyNameLabel() {
        return companyNameLabel;
    }

    public static Label getAgeOfYoungesLabel() {
        return ageOfYoungesLabel;
    }

    public static Label getPhoneNrLabel() {
        return phoneNrLabel;
    }

    public static Label getNoOfPeopleLabel() {
        return noOfPeopleLabel;
    }

    public static HBox getButtonHbox() {
        return buttonHbox;
    }

    public static HBox gethBox() {
        return hBox;
    }

    public static VBox getLabelVbox() {
        return labelVbox;
    }

    public static VBox getTextVbox() {
        return textVbox;
    }

    public static BorderPane getBorderPane() {
        return borderPane;
    }

    public static Button getNextButton() {
        return nextButton;
    }

    public static Button getBackButton() {
        return backButton;
    }
}
