package sample.Presentation;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import sample.Application.DataTypes.Activities;

import static javafx.application.Application.launch;

public class OverViewScene {
    private static ListView<String> listView;
    private static Button addButton, bookButton, deleteButton, backButton;
    private static HBox hBoxButtonBarActivities;
    private static Label headerActivitiesLabel;
    private static BorderPane pane;
    private static Scene scene;


    public static Scene initialize() {
        listView = new ListView<String>();
        ObservableList<String> items = FXCollections.observableArrayList();
        for(Activities a: Main.activities){
            items.add(a.getName());
        }

        listView.setItems(items);

        /*listView = new ListView();
        listView.getItems().addAll("Sumo Wrestling", "Minigolf", "PaintBall" , "Go Cart");*/
        listView.setEditable(true);
        addButton = new Button("Add");
        bookButton = new Button("Book");
        deleteButton = new Button("Delete");
        backButton = new Button("Back");

        hBoxButtonBarActivities = new HBox(40);
        hBoxButtonBarActivities.setAlignment(Pos.CENTER);
        hBoxButtonBarActivities.getChildren().addAll(backButton, addButton, bookButton, deleteButton);

        headerActivitiesLabel = new Label("Activities");
        headerActivitiesLabel.setId("individualLabel");

        VBox vBox = new VBox(30);
        vBox.getChildren().addAll(headerActivitiesLabel, listView);
        pane = new BorderPane();
        pane.setAlignment(headerActivitiesLabel, Pos.CENTER);
        pane.setTop(headerActivitiesLabel);
        pane.setCenter(listView);
        pane.setAlignment(hBoxButtonBarActivities, Pos.CENTER);
        pane.setBottom(hBoxButtonBarActivities);

        scene = new Scene(pane, 1600, 600);
        scene.getStylesheets().add("style.css");
        return scene;

    }

    public static ListView<String> getListView() {
        return listView;
    }

    public static Button getAddButton() {
        return addButton;
    }

    public static Button getBookButton() {
        return bookButton;
    }

    public static Button getDeleteButton() {
        return deleteButton;
    }

    public static HBox gethBoxButtonBarActivities() {
        return hBoxButtonBarActivities;
    }

    public static Label getHeaderActivitiesLabel() {
        return headerActivitiesLabel;
    }

    public static BorderPane getPane() {
        return pane;
    }

    public static Scene getScene() {
        return scene;
    }

    public static Button getBackButton() {
        return backButton;
    }
}
