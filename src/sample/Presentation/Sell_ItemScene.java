package sample.Presentation;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class Sell_ItemScene {
    private static Scene scene;
    private static VBox labelsVBox, textFieldsVBox;
    private static HBox buttonsTogetherHBox, labelsAndTFTogether;
    private static BorderPane borderPane;
    private static Label titleLabel, itemLabel, priceLabel, quantityLabel;
    private static ChoiceBox<String> choiceBox;
    private static TextField priceField, quantityField;
    private static Button backButton, addItemButton, skipButton;

    public static Scene initialize() {
        // labels
        titleLabel = new Label("Sell Item");
        titleLabel.setId("individualLabel");
        itemLabel = new Label("Item: ");
        priceLabel = new Label("Price: ");
        quantityLabel = new Label("Quantity: ");

        // choice box
        choiceBox = new ChoiceBox<>();
        //choiceBox.getItems().addAll("T-shirts", "Candies", "Soda");

        // text fields
        priceField = new TextField();
        quantityField = new TextField();

        // buttons
        backButton = new Button("Back");
        addItemButton = new Button("Add Item");
        skipButton = new Button("Skip");

        // putting labels together
        labelsVBox = new VBox(20);
        labelsVBox.getChildren().addAll(itemLabel, priceLabel, quantityLabel);

        // putting choice box and text fields together
        textFieldsVBox = new VBox(10);
        textFieldsVBox.getChildren().addAll(choiceBox, priceField, quantityField);

        // putting labels, choice box and text fields together
        labelsAndTFTogether = new HBox(20);
        labelsAndTFTogether.getChildren().addAll(labelsVBox, textFieldsVBox);

        // putting the buttons together
        buttonsTogetherHBox = new HBox(20);
        buttonsTogetherHBox.getChildren().addAll(backButton, addItemButton, skipButton);

        // setting the main pane
        borderPane = new BorderPane();
        borderPane.setPadding(new Insets(20));
        borderPane.setTop(titleLabel);
        titleLabel.setPadding(new Insets(0, 0, 20, 0));
        borderPane.setAlignment(titleLabel, Pos.CENTER);
        borderPane.setCenter(labelsAndTFTogether);
        borderPane.setBottom(buttonsTogetherHBox);

        borderPane.getStylesheets().add("style.css");

        scene = new Scene(borderPane, 400, 300);

        return scene;

    }

    public static Scene getScene() {
        return scene;
    }

    public static VBox getLabelsVBox() {
        return labelsVBox;
    }

    public static VBox getTextFieldsVBox() {
        return textFieldsVBox;
    }

    public static HBox getButtonsTogetherHBox() {
        return buttonsTogetherHBox;
    }

    public static HBox getLabelsAndTFTogether() {
        return labelsAndTFTogether;
    }

    public static BorderPane getBorderPane() {
        return borderPane;
    }

    public static Label getTitleLabel() {
        return titleLabel;
    }

    public static Label getItemLabel() {
        return itemLabel;
    }

    public static Label getPriceLabel() {
        return priceLabel;
    }

    public static Label getQuantityLabel() {
        return quantityLabel;
    }

    public static ChoiceBox<String> getChoiceBox() {
        return choiceBox;
    }

    public static TextField getPriceField() {
        return priceField;
    }

    public static TextField getQuantityField() {
        return quantityField;
    }

    public static Button getBackButton() {
        return backButton;
    }

    public static Button getAddItemButton() {
        return addItemButton;
    }

    public static Button getSkipButton() {
        return skipButton;
    }
}