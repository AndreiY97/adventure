package sample.Presentation;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class IndividualBooking{

    private static Label title, firstName, lastName, ageOfYoungest, phoneNumber, noOfPeople;
    private static TextField firstNameField, lastNameField, ageField, phoneField, noOfPeopleField;
    private static Button nextButton, backButton;
    private static Scene individualBookingScene;
    private static VBox vboxLabel, vboxTextFields;
    private  static HBox hboxButtons, hboxLabelsAndFields;
    private static BorderPane borderPane;

    public static Scene initialize(){

        title = new Label("Individual Booking");
        title.setId("individualLabel");
        title.setPadding(new Insets(-10,0,15,0));
        firstName = new Label("First Name: ");
        firstNameField = new TextField();
        firstNameField.setPromptText("Enter first name");

        lastName = new Label("Last name: ");
        lastNameField = new TextField();
        lastNameField.setPromptText("Enter last name");

        ageOfYoungest = new Label("Age Of Youngest: ");
        ageField = new TextField();
        ageField.setPromptText("Enter age");
        /*int age = Integer.parseInt(ageField.getText());
        System.out.println("Age: "  + age);
        if (age < 10){
            System.out.println("AGE < 10");
        }*/

        phoneNumber = new Label("Phone: ");
        phoneField = new TextField();
        phoneField.setPromptText("Enter phone number");

        noOfPeople = new Label("Number Of People: ");
        noOfPeopleField = new TextField();
        noOfPeopleField.setPromptText("Enter number of people");

        vboxLabel = new VBox(24);
        vboxLabel.getChildren().addAll(firstName, lastName, ageOfYoungest,noOfPeople, phoneNumber);
        vboxTextFields = new VBox(14);
        vboxTextFields.getChildren().addAll(firstNameField, lastNameField, ageField,noOfPeopleField, phoneField);

        backButton = new Button("Back");
        nextButton = new Button("Next");

        hboxButtons = new HBox(20);
        hboxButtons.setAlignment(Pos.BOTTOM_RIGHT);
        hboxButtons.getChildren().addAll(backButton, nextButton);

        hboxLabelsAndFields = new HBox(15);
        hboxLabelsAndFields.getChildren().addAll(vboxLabel, vboxTextFields);

        borderPane = new BorderPane();

        borderPane.setPadding(new Insets(20));
        borderPane.setAlignment(title, Pos.CENTER);
        borderPane.setTop(title);
        borderPane.setCenter(hboxLabelsAndFields);
        borderPane.setBottom(hboxButtons);


        individualBookingScene = new Scene(borderPane, 1600, 600);


        individualBookingScene.getStylesheets().add("style.css");



        return individualBookingScene;
    }
    public static Label getTitle() {
        return title;
    }

    public static Label getFirstName() {
        return firstName;
    }

    public static Label getLastName() {
        return lastName;
    }

    public static Label getAgeOfYoungest() {
        return ageOfYoungest;
    }

    public static Label getPhoneNumber() {
        return phoneNumber;
    }

    public static Label getNoOfPeople() {
        return noOfPeople;
    }

    public static TextField getFirstNameField() {
        return firstNameField;
    }

    public static TextField getLastNameField() {
        return lastNameField;
    }

    public static TextField getAgeField() {
        return ageField;
    }

    public static TextField getPhoneField() {
        return phoneField;
    }

    public static TextField getNoOfPeopleField() {
        return noOfPeopleField;
    }

    public static Button getNextButton() {
        return nextButton;
    }

    public static Button getBackButton() {
        return backButton;
    }

    public static Scene getIndividualBookingScene() {
        return individualBookingScene;
    }

    public static VBox getVboxLabel() {
        return vboxLabel;
    }

    public static VBox getVboxTextFields() {
        return vboxTextFields;
    }

    public static HBox getHboxButtons() {
        return hboxButtons;
    }

    public static BorderPane getBorderPane() {
        return borderPane;
    }
}
