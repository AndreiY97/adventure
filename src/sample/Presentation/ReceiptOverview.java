package sample.Presentation;


import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class ReceiptOverview {
    private static Label titleLabel, customerLabel, noOfPeople, pricePerPerson, time, total, customerName, noOfPeopleInfo, priceInfo, timeInfo, totalPrice;
    private static Button backButton, bookButton;
    private static VBox wholeScene, labelVBox, infoVBox;
    private static HBox labelsTogether, buttonsTogether;
    private static Scene receiptOverviewScene;

    public static Scene initialize() {
        titleLabel = new Label("Receipt");
        titleLabel.setId("individualLabel");
        customerLabel = new Label("Customer: ");
        noOfPeople = new Label("No. of people: ");
        pricePerPerson = new Label("Price per person: ");
        time = new Label("Time: ");
        total = new Label("Total: ");
        customerName = new Label();
        noOfPeopleInfo = new Label();

        priceInfo = new Label(ActivityDetails.getPriceField().getText());
        timeInfo = new Label("Time info");
        totalPrice = new Label("Total price is...");

        backButton = new Button("Back");
        bookButton = new Button("Book");

        labelVBox = new VBox(10);
        labelVBox.getChildren().addAll(customerLabel, noOfPeople, pricePerPerson, time, total);

        infoVBox = new VBox(10);
        infoVBox.getChildren().addAll(customerName, noOfPeopleInfo, priceInfo, timeInfo, totalPrice);

        labelsTogether = new HBox(20);
        labelsTogether.getChildren().addAll(labelVBox, infoVBox);

        buttonsTogether = new HBox(20);
        buttonsTogether.getChildren().addAll(backButton, bookButton);

        wholeScene = new VBox(20);
        wholeScene.getChildren().addAll(titleLabel, labelsTogether, buttonsTogether);
        wholeScene.setPadding(new Insets(20));
        wholeScene.setAlignment(Pos.CENTER);
        receiptOverviewScene = new Scene(wholeScene, 1600, 600);
        receiptOverviewScene.getStylesheets().add("style.css");

        return receiptOverviewScene;
    }

    public static Label getTitleLabel() {
        return titleLabel;
    }

    public static Label getCustomerLabel() {
        return customerLabel;
    }

    public static Label getNoOfPeople() {
        return noOfPeople;
    }

    public static Label getPricePerPerson() {
        return pricePerPerson;
    }

    public static Label getTime() {
        return time;
    }

    public static Label getTotal() {
        return total;
    }

    public static Label getCustomerName() {
        return customerName;
    }

    public static Label getNoOfPeopleInfo() {
        return noOfPeopleInfo;
    }

    public static Label getPriceInfo() {
        return priceInfo;
    }

    public static Label getTimeInfo() {
        return timeInfo;
    }

    public static Label getTotalPrice() {
        return totalPrice;
    }

    public static Button getBackButton() {
        return backButton;
    }

    public static Button getBookButton() {
        return bookButton;
    }

    public static VBox getWholeScene() {
        return wholeScene;
    }

    public static VBox getLabelVBox() {
        return labelVBox;
    }

    public static VBox getInfoVBox() {
        return infoVBox;
    }

    public static HBox getLabelsTogether() {
        return labelsTogether;
    }

    public static HBox getButtonsTogether() {
        return buttonsTogether;
    }

    public static Scene getReceiptOverviewScene() {
        return receiptOverviewScene;
    }
}
