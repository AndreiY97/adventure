package sample.Presentation;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;

public class ChoiceView {
    private static Button seeBookings, seeActivities, seeItemsToSell;
    private static Label choiceViewLabel;
    private static HBox hBox;
    private static BorderPane borderPane;
    private static Scene choiceScene;

    public static Scene initialize(){
        choiceViewLabel = new Label("Activities");
        choiceViewLabel.setId("individualLabel");
        seeBookings = new Button("See bookings");
        seeActivities = new Button("See activities");
        seeItemsToSell = new Button("Items to sell");
        hBox = new HBox(45);
        hBox.setAlignment(Pos.CENTER);
        hBox.getChildren().addAll(seeBookings, seeActivities, seeItemsToSell);
        borderPane = new BorderPane();

        borderPane.setPadding(new Insets(20));

        borderPane.setCenter(hBox);
        borderPane.setAlignment(choiceViewLabel, Pos.CENTER);

        borderPane.setTop(choiceViewLabel);
        choiceScene = new Scene(borderPane, 1600, 600);
        choiceScene.getStylesheets().add("style.css");

        return choiceScene;

    }

    public static Button getSeeBookings() {
        return seeBookings;
    }

    public static Button getSeeActivities() {
        return seeActivities;
    }

    public static HBox gethBox() {
        return hBox;
    }

    public static Scene getChoiceScene() {
        return choiceScene;
    }

    public static Button getSeeItemsToSell() {
        return seeItemsToSell;
    }
}
