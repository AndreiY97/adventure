package sample.Presentation;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class BookingScene {

    private static Button company, individual, cancel;
    private static Label bookingTypeLabel;
    private static BorderPane borderPane;
    private static Scene bookingScene;
    private static VBox vBox;
    private static HBox hBox;


    public static Scene initialize() {
        //Buttons
        company = new Button("Company");
        individual = new Button("Individual");
        cancel = new Button("Cancel");

        hBox = new HBox(100);
        hBox.setAlignment(Pos.CENTER);
        hBox.getChildren().addAll(individual, company);

        //Label
        bookingTypeLabel = new Label("Choose your booking type");
        bookingTypeLabel.setId("individualLabel");

        //pane
        borderPane = new BorderPane();

        vBox = new VBox(40);
        vBox.setAlignment(Pos.CENTER);

        vBox.getChildren().addAll(bookingTypeLabel, hBox, cancel);

        borderPane.setCenter(vBox);

        bookingScene = new Scene(borderPane, 1600, 600);

        bookingScene.getStylesheets().add("style.css");

        return bookingScene;


    }

    public static Button getCompany() {
        return company;
    }

    public static Button getIndividual() {
        return individual;
    }

    public static Button getCancel() {
        return cancel;
    }

    public static Label getBookingTypeLabel() {
        return bookingTypeLabel;
    }

    public static BorderPane getBorderPane() {
        return borderPane;
    }

    public static Scene getBookingScene() {
        return bookingScene;
    }
}
