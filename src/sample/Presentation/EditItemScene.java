package sample.Presentation;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class EditItemScene {
    private static Label labelEditItem, labelName, labelQuantity, labelPrice;
    private static TextField itemNameTextField, quantityTextField, priceTextField;
    private static Button backButton, saveButton;
    private static VBox labelsVBox, textFieldsVBox;
    private static HBox labelsAndTextFieldsHBox, buttonsHBox;
    private static BorderPane borderPane;
    private static Scene editItemScene;

    public static Scene initialize() {
        // Buttons
        backButton = new Button("Back");
        saveButton = new Button("Save");

        // Labels
        labelEditItem = new Label("");
        labelEditItem.setId("individualLabel");
        labelName = new Label("Name: ");

        labelQuantity = new Label("Quantity: ");
        labelPrice = new Label("Price: ");

        // Textfields
        itemNameTextField = new TextField();
        quantityTextField = new TextField();
        priceTextField = new TextField();

        // Adding the labels to a VBox
        labelsVBox = new VBox(19);
        labelsVBox.getChildren().addAll(labelName, labelPrice, labelQuantity);

        // Adding the textFields to a VBox
        textFieldsVBox = new VBox(10);
        textFieldsVBox.getChildren().addAll(itemNameTextField, priceTextField, quantityTextField);

        // Putting the VBoxes together side by side
        labelsAndTextFieldsHBox = new HBox(20);
        labelsAndTextFieldsHBox.getChildren().addAll(labelsVBox, textFieldsVBox);

        // Putting the buttons in a HBox
        buttonsHBox = new HBox(20);
        buttonsHBox.getChildren().addAll(backButton, saveButton);

        // Creating and setting the Border Pane
        borderPane = new BorderPane();
        borderPane.setPadding(new Insets(20));

        borderPane.setTop(labelEditItem);
        borderPane.setCenter(labelsAndTextFieldsHBox);
        borderPane.setBottom(buttonsHBox);

        borderPane.setAlignment(labelEditItem, Pos.CENTER);
        labelEditItem.setPadding(new Insets(0, 0, 30, 0));

        labelsAndTextFieldsHBox.setAlignment(Pos.CENTER);
        buttonsHBox.setAlignment(Pos.CENTER);

        editItemScene = new Scene(borderPane, 1600, 600);


        editItemScene.getStylesheets().add("style.css");

        return editItemScene;
    }

    public static void setLabelEditItem(Label labelEditItem) {
        EditItemScene.labelEditItem = labelEditItem;
    }

    public static void setLabelName(Label labelName) {
        EditItemScene.labelName = labelName;
    }

    public static void setLabelQuantity(Label labelQuantity) {
        EditItemScene.labelQuantity = labelQuantity;
    }

    public static void setLabelPrice(Label labelPrice) {
        EditItemScene.labelPrice = labelPrice;
    }

    public static void setItemNameTextField(TextField itemNameTextField) {
        EditItemScene.itemNameTextField = itemNameTextField;
    }

    public static void setQuantityTextField(TextField quantityTextField) {
        EditItemScene.quantityTextField = quantityTextField;
    }

    public static void setPriceTextField(TextField priceTextField) {
        EditItemScene.priceTextField = priceTextField;
    }

    public static void setBackButton(Button backButton) {
        EditItemScene.backButton = backButton;
    }

    public static void setSaveButton(Button saveButton) {
        EditItemScene.saveButton = saveButton;
    }

    public static void setLabelsVBox(VBox labelsVBox) {
        EditItemScene.labelsVBox = labelsVBox;
    }

    public static void setTextFieldsVBox(VBox textFieldsVBox) {
        EditItemScene.textFieldsVBox = textFieldsVBox;
    }

    public static void setLabelsAndTextFieldsHBox(HBox labelsAndTextFieldsHBox) {
        EditItemScene.labelsAndTextFieldsHBox = labelsAndTextFieldsHBox;
    }

    public static void setButtonsHBox(HBox buttonsHBox) {
        EditItemScene.buttonsHBox = buttonsHBox;
    }

    public static void setBorderPane(BorderPane borderPane) {
        EditItemScene.borderPane = borderPane;
    }

    public static void setEditItemScene(Scene editItemScene) {
        EditItemScene.editItemScene = editItemScene;
    }

    public static Label getLabelEditItem() {
        return labelEditItem;
    }

    public static Label getLabelName() {
        return labelName;
    }

    public static Label getLabelQuantity() {
        return labelQuantity;
    }

    public static Label getLabelPrice() {
        return labelPrice;
    }

    public static TextField getItemNameTextField() {
        return itemNameTextField;
    }

    public static TextField getQuantityTextField() {
        return quantityTextField;
    }

    public static TextField getPriceTextField() {
        return priceTextField;
    }

    public static Button getBackButton() {
        return backButton;
    }

    public static Button getSaveButton() {
        return saveButton;
    }

    public static VBox getLabelsVBox() {
        return labelsVBox;
    }

    public static VBox getTextFieldsVBox() {
        return textFieldsVBox;
    }

    public static HBox getLabelsAndTextFieldsHBox() {
        return labelsAndTextFieldsHBox;
    }

    public static HBox getButtonsHBox() {
        return buttonsHBox;
    }

    public static BorderPane getBorderPane() {
        return borderPane;
    }

    public static Scene getEditItemScene() {
        return editItemScene;
    }
}
