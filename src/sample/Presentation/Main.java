package sample.Presentation;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import sample.Application.DataTypes.Activities;
import sample.Application.DataTypes.Control.MainControl;

public class Main extends Application {

    public static ObservableList<Activities> activities = FXCollections.observableArrayList();

    @Override
    public void start(Stage primaryStage) throws Exception{
        activities.add(new Activities(1,"Sumo Wrestling", 50, 100));
        activities.add(new Activities(2,"Minigolf", 70, 150));
        activities.add(new Activities(3,"PaintBall", 80, 30));
        activities.add(new Activities(4,"Go Cart", 80, 40));
       MainControl.showLoginScene();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
