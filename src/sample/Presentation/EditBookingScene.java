package sample.Presentation;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class EditBookingScene {
    private static Label labelEditActivity, labelName, labelActivity, labelNoOfPeople,
            labelTypeOfReservation, labelDate, labelTime, labelTel, labelAge, labelPrice;
    private static TextField textFieldName, textFieldActivity, textFieldNoOfPeople, textFieldTypeOfReservation, textFieldDate,
            textFieldTime, textFieldTel, textFieldAge, textFieldPrice;
    private static Button backButton, saveButton;
    private static VBox labelsVBox, textFieldsVBox;
    private static HBox labelsAndTextFieldsHBox, buttonsHBox;
    private static BorderPane borderPane;
    private static Scene editBookingScene;

    public static Scene initialize() {
        // Buttons
        backButton = new Button("Back");
        saveButton = new Button("Save");

        // Labels
        labelEditActivity = new Label("Edit Activity");
        labelName = new Label("Name: ");
        labelActivity = new Label("Activity: ");
        labelNoOfPeople = new Label("Number of people: ");
        labelTypeOfReservation = new Label("Reservation Type: ");
        labelDate = new Label("Date: ");
        labelTime = new Label("Time: ");
        labelTel = new Label("Phone: ");
        labelAge = new Label("Age: ");
        labelPrice = new Label("Price: ");

        // Textfields
        textFieldName = new TextField();
        textFieldActivity = new TextField();
        textFieldNoOfPeople = new TextField();
        textFieldTypeOfReservation = new TextField();
        textFieldDate = new TextField();
        textFieldTime = new TextField();
        textFieldTel = new TextField();
        textFieldAge = new TextField();
        textFieldPrice = new TextField();

        // Adding the labels to a VBox
        labelsVBox = new VBox(19);
        labelsVBox.getChildren().addAll(labelName, labelActivity, labelNoOfPeople, labelTypeOfReservation, labelDate,
                labelTime, labelTel, labelAge, labelPrice);

        // Adding the textFields to a VBox
        textFieldsVBox = new VBox(10);
        textFieldsVBox.getChildren().addAll(textFieldName, textFieldActivity, textFieldNoOfPeople, textFieldTypeOfReservation,
                textFieldDate, textFieldTime, textFieldTel, textFieldAge, textFieldPrice);

        // Putting the VBoxes together side by side
        labelsAndTextFieldsHBox = new HBox(20);
        labelsAndTextFieldsHBox.getChildren().addAll(labelsVBox, textFieldsVBox);

        // Putting the buttons in a HBox
        buttonsHBox = new HBox(20);
        buttonsHBox.getChildren().addAll(backButton, saveButton);

        // Creating and setting the Border Pane
        borderPane = new BorderPane();
        borderPane.setPadding(new Insets(20));

        borderPane.setTop(labelEditActivity);
        borderPane.setCenter(labelsAndTextFieldsHBox);
        borderPane.setBottom(buttonsHBox);

        borderPane.setAlignment(labelEditActivity, Pos.CENTER);
        labelEditActivity.setPadding(new Insets(0, 0, 30, 0));

        labelsAndTextFieldsHBox.setAlignment(Pos.CENTER);
        buttonsHBox.setAlignment(Pos.CENTER);

        editBookingScene = new Scene(borderPane, 1600, 600);


        editBookingScene.getStylesheets().add("style.css");

        return editBookingScene;
    }

    public static Label getLabelEditActivity() {
        return labelEditActivity;
    }

    public static Label getLabelName() {
        return labelName;
    }

    public static Label getLabelActivity() {
        return labelActivity;
    }

    public static Label getLabelNoOfPeople() {
        return labelNoOfPeople;
    }

    public static Label getLabelTypeOfReservation() {
        return labelTypeOfReservation;
    }

    public static Label getLabelDate() {
        return labelDate;
    }

    public static Label getLabelTime() {
        return labelTime;
    }

    public static Label getLabelTel() {
        return labelTel;
    }

    public static Label getLabelAge() {
        return labelAge;
    }

    public static Label getLabelPrice() {
        return labelPrice;
    }

    public static TextField getTextFieldName() {
        return textFieldName;
    }

    public static TextField getTextFieldActivity() {
        return textFieldActivity;
    }

    public static TextField getTextFieldNoOfPeople() {
        return textFieldNoOfPeople;
    }

    public static TextField getTextFieldTypeOfReservation() {
        return textFieldTypeOfReservation;
    }

    public static TextField getTextFieldDate() {
        return textFieldDate;
    }

    public static TextField getTextFieldTime() {
        return textFieldTime;
    }

    public static TextField getTextFieldTel() {
        return textFieldTel;
    }

    public static TextField getTextFieldAge() {
        return textFieldAge;
    }

    public static TextField getTextFieldPrice() {
        return textFieldPrice;
    }

    public static Button getBackButton() {
        return backButton;
    }

    public static Button getSaveButton() {
        return saveButton;
    }

    public static VBox getLabelsVBox() {
        return labelsVBox;
    }

    public static VBox getTextFieldsVBox() {
        return textFieldsVBox;
    }

    public static HBox getLabelsAndTextFieldsHBox() {
        return labelsAndTextFieldsHBox;
    }

    public static HBox getButtonsHBox() {
        return buttonsHBox;
    }

    public static BorderPane getBorderPane() {
        return borderPane;
    }

    public static Scene getEditBookingScene() {
        return editBookingScene;
    }
}
