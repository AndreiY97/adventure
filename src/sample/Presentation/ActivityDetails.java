package sample.Presentation;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * Created by Administrator on 9/29/2016.
 */
public class ActivityDetails {

    private static Scene scene;
    private static BorderPane pane;
    private static Label dateLabel, timeLabel, remainingCapacityLabel, maxCapacityLabel, priceLabel, title;
    private static DatePicker datePicker;
    private static ChoiceBox<Integer> choiceBox;
    private static VBox labelVbox;
    private static VBox vBox, fieldsAndButtons;
    private static TextField priceField, remainingCapacityField, maxCapacityField;
    private static ImageView image;
    private static Button backButton, editButton, continueButton;
    private static HBox buttonLayout, labelsAndFields;

    public static Scene initialize(){

        //labels initialize
        title = new Label("");
        title.setId("individualLabel");
        dateLabel = new javafx.scene.control.Label("Date:");
        timeLabel = new Label("Hour(s):");
        remainingCapacityLabel = new Label("Remaining Capacity:");
        priceLabel = new Label("Price:");
        maxCapacityLabel = new Label("Max Capacity:");

        //initialize datePicker
        datePicker = new DatePicker();
        choiceBox = new ChoiceBox<Integer>();

        //textfields
        priceField = new TextField();
        //priceField.setDisable(true);

        remainingCapacityField = new TextField();
        //remainingCapacityField.setDisable(true);

        maxCapacityField = new TextField();
        // maxCapacityField.setDisable(true);
        //initalize label vbox
        labelVbox = new VBox(25);
        labelVbox.getChildren().addAll(dateLabel, timeLabel, priceLabel, remainingCapacityLabel, maxCapacityLabel);


        //intialize vbox
        vBox = new VBox(14);
        vBox.getChildren().addAll(datePicker,choiceBox, priceField, remainingCapacityField, maxCapacityField);

        pane = new BorderPane();
        pane.setAlignment(title, Pos.CENTER);
        pane.setTop(title);
        labelsAndFields = new HBox(15);
        labelsAndFields.getChildren().addAll(labelVbox, vBox);
        editButton = new Button("Save");
        editButton.setMaxWidth(Double.MAX_VALUE);
        backButton = new Button("Back");
        backButton.setMaxWidth(Double.MAX_VALUE);
        continueButton = new Button("Continue");
        continueButton.setMaxWidth(Double.MAX_VALUE);
        buttonLayout = new HBox(25);
        fieldsAndButtons = new VBox(30);

        fieldsAndButtons.getChildren().addAll(labelsAndFields, buttonLayout);

        buttonLayout.getChildren().addAll(backButton, editButton, continueButton);
        buttonLayout.setSpacing(75);
        pane.setPadding(new Insets(20));
        pane.setCenter(fieldsAndButtons);

        scene = new Scene(pane, 1600, 600);
        scene.getStylesheets().add("style.css");
        return scene;
    }

    public static BorderPane getPane() {
        return pane;
    }

    public static Label getRemainingCapacityLabel() {
        return remainingCapacityLabel;
    }

    public static Label getMaxCapacityLabel() {
        return maxCapacityLabel;
    }

    public static Label getPriceLabel() {
        return priceLabel;
    }

    public static VBox getFieldsAndButtons() {
        return fieldsAndButtons;
    }

    public static TextField getPriceField() {
        return priceField;
    }

    public static TextField getRemainingCapacityField() {
        return remainingCapacityField;
    }

    public static TextField getMaxCapacityField() {
        return maxCapacityField;
    }

    public static HBox getLabelsAndFields() {
        return labelsAndFields;
    }

    public static Button getBackButton() {
        return backButton;
    }

    public static Button getEditButton() {
        return editButton;
    }

    public static HBox getButtonLayout() {
        return buttonLayout;
    }

    public static Scene getScene() {
        return scene;
    }


    public static Label getDateLabel() {
        return dateLabel;
    }

    public static Label getTimeLabel() {
        return timeLabel;
    }

    public static DatePicker getDatePicker() {
        return datePicker;
    }

    public static ChoiceBox<Integer> getChoiceBox() {
        return choiceBox;
    }

    public static VBox getLabelVbox() {
        return labelVbox;
    }

    public static VBox getvBox() {
        return vBox;
    }

    public static ImageView getImage() {
        return image;
    }

    public static Button getContinueButton() {
        return continueButton;
    }

    public static Label getTitle() {
        return title;
    }
}