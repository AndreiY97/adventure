package sample.Presentation;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import sample.Application.DataTypes.Control.MainControl;

public class LoginScene {
    private static Scene scene;
    private static VBox loginLayout;
    private static Label loginLabel;
    private static Label adventureLabel;
    private static TextField usernameField;
    private static PasswordField passwordField;
    private static Button loginButton;
    private static String username;
    private static String password;

    //initialization of objects
    public static void initialize(){

        //loginLabel
        loginLabel = new Label("Log in");
        loginLabel.setPadding((new Insets(0, 0, 30, 0)));
        loginLabel.setStyle("-fx-font-size: 24pt");


        //adventureLabel
        adventureLabel = new Label("Adventure Alley");
        adventureLabel.setId("welcome");

        //usernameField
        usernameField = new TextField();
        usernameField.setPromptText("User ID");
        usernameField.setFocusTraversable(false);
        usernameField.setMaxWidth(220);
        usernameField.setAlignment(Pos.CENTER);

        //passwordField
        passwordField = new PasswordField();
        passwordField.setAlignment(Pos.CENTER);
        passwordField.setMaxWidth(220);
        passwordField.setPromptText("Password");
        passwordField.setFocusTraversable(false);

        //buttonField
        loginButton = new Button("Login");
        loginButton.setMaxWidth(120);

        //layout
        loginLayout = new VBox(10);
        loginLayout.setAlignment(Pos.CENTER);
        loginLayout.getChildren().addAll(adventureLabel,loginLabel,usernameField,passwordField,loginButton);

        //scene
        scene = new Scene(loginLayout,1600, 600);
        scene.getStylesheets().add("style.css");

    }

    //initialize
    public static void initialize1() {
        //username field
        usernameField = LoginScene.getUsernameField();

        //password field
        passwordField = LoginScene.getPasswordField();


        //loginButton
        loginButton = LoginScene.getLoginButton();
        loginButton.setDefaultButton(true);
        loginButton.setOnAction(e -> {
            //username
            username = usernameField.getText();

            //password
            password = passwordField.getText();

            handle_loginButton();

        });

    }


    //handle login button
    public static void handle_loginButton() {
        int ok = 0;

        if ((username.equals("a") || username.equals("b")) && password.equals("1") ){
            MainControl.showChoiceView();
            System.out.println("Login Successful ..... ChoiceView displayed ");

        }

    }

    //getters
    public static Scene getScene() {
        return scene;
    }

    public static VBox getLoginLayout() {
        return loginLayout;
    }

    public static Label getLoginLabel() {
        return loginLabel;
    }

    public static Label getAdventureLabel() {
        return adventureLabel;
    }

    public static TextField getUsernameField() {
        return usernameField;
    }

    public static PasswordField getPasswordField() {
        return passwordField;
    }

    public static Button getLoginButton() {
        return loginButton;
    }
}
