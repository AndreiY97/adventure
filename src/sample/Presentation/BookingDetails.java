package sample.Presentation;

import com.sun.javafx.scene.control.TableColumnComparatorBase;
import com.sun.javafx.scene.control.skin.DatePickerContent;
import com.sun.javafx.scene.control.skin.DatePickerSkin;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import sample.Application.DataTypes.BookingsTable;
import sample.Application.DataTypes.Reserved_act;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class BookingDetails {
    private static TableView <BookingsTable> bookingDetails;
    private static TableColumn<BookingsTable, String> name;
    private static TableColumn<BookingsTable, String> activity;
    private static TableColumn<BookingsTable, String> noOfPeople;
    private static TableColumn<BookingsTable, String> typeOfReservation;
    private static TableColumn<BookingsTable, String> date;
    private static TableColumn<BookingsTable, String> time;
    private static TableColumn<BookingsTable, String> tel;
    private static TableColumn<BookingsTable, String> age;
    private static TableColumn<BookingsTable, String> price;
    private static BorderPane pane;
    private static Button cancelButton, backButton, checkButton, resetButton, editButton, searchButton;
    private static HBox hBox, calendarButtons, searchHBox;
    private static VBox vBox, headerPlusSearch;
    private static Scene bookingDetailsScene;
    private static Label headerLabel;
    private static TextField searchField;
    private static DatePickerContent pop;

    public static Scene initialize(){
        bookingDetails = new TableView<>();
        bookingDetails.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        bookingDetails.setId("myTableView");
        //bookingDetails.columnResizePolicyProperty(TableView.CONSTRAINED_RESIZE_POLICY);
        DatePickerSkin datePickerSkin = new DatePickerSkin(new DatePicker(LocalDate.now()));
        pop = (DatePickerContent)datePickerSkin.getPopupContent();

        //  Node popupContent = datePickerSkin.getPopupContent();
      //  LocalDate date = popupContent.getUserData();

        headerLabel = new Label("Bookings");
        headerLabel.setId("individualLabel");
        headerLabel.setPadding(new Insets(7, 340, 0, 400));
        checkButton = new Button("Check");
        resetButton = new Button("Reset");
        searchButton = new Button("Search");
        searchField = new TextField();
        searchField.setPromptText("Search");
        searchHBox = new HBox(10);
        searchHBox.setPadding(new Insets(15, 400, 10, 10));
        searchHBox.getChildren().addAll(searchField, searchButton);
        calendarButtons = new HBox(20);
        calendarButtons.setAlignment(Pos.BASELINE_CENTER);
        calendarButtons.getChildren().addAll(checkButton, resetButton);
        vBox = new VBox(15);
        vBox.getChildren().addAll(pop, calendarButtons);
        headerPlusSearch = new VBox(10);
        headerPlusSearch.getChildren().addAll(headerLabel, searchHBox);
        //vBox.setAlignment(Pos.BASELINE_CENTER);
        vBox.setPadding(new Insets(75, 10, 0, 10));

        name = new TableColumn<>("Name");
        name.setCellValueFactory(cellData -> cellData.getValue().customerNameDBProperty());
        name.setCellFactory(TextFieldTableCell.forTableColumn());

        activity = new TableColumn<>("Activity");
        activity.setCellValueFactory(cellData -> cellData.getValue().activityNameDBProperty());
        activity.setCellFactory(TextFieldTableCell.forTableColumn());

        noOfPeople = new TableColumn<>("# People");
        noOfPeople.setCellValueFactory(cellData -> cellData.getValue().numberOfPeopleProperty());
        noOfPeople.setCellFactory(TextFieldTableCell.forTableColumn());

        //noOfPeople.prefWidthProperty().bind(bookingDetails.widthProperty().multiply(0.15));
        typeOfReservation = new TableColumn<>("Booking Type");
        typeOfReservation.setCellValueFactory(cellData -> cellData.getValue().customerTypeDBProperty());
        typeOfReservation.setCellFactory(TextFieldTableCell.forTableColumn());

        //typeOfReservation.prefWidthProperty().bind(bookingDetails.widthProperty().multiply(0.15));
        date = new TableColumn<>("Date");
        date.setCellValueFactory(cellData -> cellData.getValue().dateDBProperty());
        date.setCellFactory(TextFieldTableCell.forTableColumn());

        time = new TableColumn<>("Hour(s)");
        time.setCellValueFactory(cellData -> cellData.getValue().timeDBProperty());
        time.setCellFactory(TextFieldTableCell.forTableColumn());

        tel = new TableColumn<>("Phone");
        tel.setCellValueFactory(cellData -> cellData.getValue().phoneDBProperty());
        tel.setCellFactory(TextFieldTableCell.forTableColumn());

        age = new TableColumn<>("Age");
        age.setCellValueFactory(cellData -> cellData.getValue().ageDBProperty());
        age.setCellFactory(TextFieldTableCell.forTableColumn());

        price = new TableColumn<>("Price");
        price.setCellValueFactory(cellData -> cellData.getValue().paymentDBProperty());
        price.setCellFactory(TextFieldTableCell.forTableColumn());


        name.setOnEditCommit((TableColumn.CellEditEvent<BookingsTable, String> event)->{
            ((BookingsTable)event.getTableView().getItems().get(event.getTablePosition().getRow())).setCustomerNameDB
                    (event.getNewValue());
        });
        activity.setOnEditCommit((TableColumn.CellEditEvent<BookingsTable, String> event)->{
            ((BookingsTable)event.getTableView().getItems().get(event.getTablePosition().getRow())).setActivityNameDB
                    (event.getNewValue());
        });
        noOfPeople.setOnEditCommit((TableColumn.CellEditEvent<BookingsTable, String> event)->{
            ((BookingsTable)event.getTableView().getItems().get(event.getTablePosition().getRow())).setNumberOfPeople
                    (event.getNewValue());
        });
        age.setOnEditCommit((TableColumn.CellEditEvent<BookingsTable, String> event)->{
            ((BookingsTable)event.getTableView().getItems().get(event.getTablePosition().getRow())).setAgeDB
                    (event.getNewValue());
        });
        tel.setOnEditCommit((TableColumn.CellEditEvent<BookingsTable, String> event)->{
            ((BookingsTable)event.getTableView().getItems().get(event.getTablePosition().getRow())).setPhoneDB
                    (event.getNewValue());
        });
        price.setOnEditCommit((TableColumn.CellEditEvent<BookingsTable, String> event)->{
            ((BookingsTable)event.getTableView().getItems().get(event.getTablePosition().getRow())).setPaymentDB
                    (event.getNewValue());
        });
        typeOfReservation.setOnEditCommit((TableColumn.CellEditEvent<BookingsTable, String> event)->{
            ((BookingsTable)event.getTableView().getItems().get(event.getTablePosition().getRow())).setCustomerTypeDB
                    (event.getNewValue());
        });
        date.setOnEditCommit((TableColumn.CellEditEvent<BookingsTable, String> event)->{
            ((BookingsTable)event.getTableView().getItems().get(event.getTablePosition().getRow())).setDateDB
                    (event.getNewValue());
        });
        time.setOnEditCommit((TableColumn.CellEditEvent<BookingsTable, String> event)->{
            ((BookingsTable)event.getTableView().getItems().get(event.getTablePosition().getRow())).setTimeDB
                    (event.getNewValue());
        });
        bookingDetails.setEditable(true);

        bookingDetails.getColumns().addAll(name, age, activity, time, date, noOfPeople, typeOfReservation, tel, price);

        cancelButton = new Button("Cancel");
        backButton = new Button("Back");
        editButton = new Button("Edit");
        hBox = new HBox(30);

        hBox.getChildren().addAll(backButton, editButton, cancelButton);
        hBox.setPadding(new Insets(5, 150, 5, 320));
        pane = new BorderPane();
        pane.setCenter(bookingDetails);
        pane.setBottom(hBox);
        pane.setRight(vBox);
        pane.setTop(headerPlusSearch);
        bookingDetailsScene = new Scene(pane, 1600, 600);
        bookingDetailsScene.getStylesheets().add("style.css");


        return bookingDetailsScene;


    }

    public static TableView getBookingDetails() {
        return bookingDetails;
    }

    public static BorderPane getPane() {
        return pane;
    }

    public static Button getCancelButton() {
        return cancelButton;
    }

    public static Button getBackButton() {
        return backButton;
    }

    public static Button getEditButton() {
        return editButton;
    }

    public static HBox gethBox() {
        return hBox;

    }

    public static Scene getBookingDetailsScene() {
        return bookingDetailsScene;
    }

    public static void setBookingDetails(TableView<BookingsTable> bookingDetails) {
        BookingDetails.bookingDetails = bookingDetails;
    }

    public static TableColumn<BookingsTable, String> getName() {
        return name;
    }

    public static void setName(TableColumn<BookingsTable, String> name) {
        BookingDetails.name = name;
    }

    public static TableColumn<BookingsTable, String> getActivity() {
        return activity;
    }

    public static void setActivity(TableColumn<BookingsTable, String> activity) {
        BookingDetails.activity = activity;
    }

    public static TableColumn<BookingsTable, String> getNoOfPeople() {
        return noOfPeople;
    }

    public static void setNoOfPeople(TableColumn<BookingsTable, String> noOfPeople) {
        BookingDetails.noOfPeople = noOfPeople;
    }

    public static TableColumn<BookingsTable, String> getTypeOfReservation() {
        return typeOfReservation;
    }

    public static void setTypeOfReservation(TableColumn<BookingsTable, String> typeOfReservation) {
        BookingDetails.typeOfReservation = typeOfReservation;
    }

    public static TableColumn<BookingsTable, String> getDate() {
        return date;
    }

    public static void setDate(TableColumn<BookingsTable, String> date) {
        BookingDetails.date = date;
    }

    public static TableColumn<BookingsTable, String> getTime() {
        return time;
    }

    public static void setTime(TableColumn<BookingsTable, String> time) {
        BookingDetails.time = time;
    }

    public static TableColumn<BookingsTable, String> getTel() {
        return tel;
    }

    public static void setTel(TableColumn<BookingsTable, String> tel) {
        BookingDetails.tel = tel;
    }

    public static TableColumn<BookingsTable, String> getAge() {
        return age;
    }

    public static void setAge(TableColumn<BookingsTable, String> age) {
        BookingDetails.age = age;
    }

    public static TableColumn<BookingsTable, String> getPrice() {
        return price;
    }

    public static void setPrice(TableColumn<BookingsTable, String> price) {
        BookingDetails.price = price;
    }

    public static void setPane(BorderPane pane) {
        BookingDetails.pane = pane;
    }

    public static void setCancelButton(Button cancelButton) {
        BookingDetails.cancelButton = cancelButton;
    }

    public static void setBackButton(Button backButton) {
        BookingDetails.backButton = backButton;
    }

    public static Button getCheckButton() {
        return checkButton;
    }

    public static void setCheckButton(Button checkButton) {
        BookingDetails.checkButton = checkButton;
    }

    public static Button getResetButton() {
        return resetButton;
    }

    public static void setResetButton(Button resetButton) {
        BookingDetails.resetButton = resetButton;
    }

    public static void setEditButton(Button editButton) {
        BookingDetails.editButton = editButton;
    }

    public static Button getSearchButton() {
        return searchButton;
    }

    public static void setSearchButton(Button searchButton) {
        BookingDetails.searchButton = searchButton;
    }

    public static void sethBox(HBox hBox) {
        BookingDetails.hBox = hBox;
    }

    public static HBox getCalendarButtons() {
        return calendarButtons;
    }

    public static void setCalendarButtons(HBox calendarButtons) {
        BookingDetails.calendarButtons = calendarButtons;
    }

    public static HBox getSearchHBox() {
        return searchHBox;
    }

    public static void setSearchHBox(HBox searchHBox) {
        BookingDetails.searchHBox = searchHBox;
    }

    public static VBox getvBox() {
        return vBox;
    }

    public static void setvBox(VBox vBox) {
        BookingDetails.vBox = vBox;
    }

    public static VBox getHeaderPlusSearch() {
        return headerPlusSearch;
    }

    public static void setHeaderPlusSearch(VBox headerPlusSearch) {
        BookingDetails.headerPlusSearch = headerPlusSearch;
    }

    public static void setBookingDetailsScene(Scene bookingDetailsScene) {
        BookingDetails.bookingDetailsScene = bookingDetailsScene;
    }

    public static Label getHeaderLabel() {
        return headerLabel;
    }

    public static void setHeaderLabel(Label headerLabel) {
        BookingDetails.headerLabel = headerLabel;
    }

    public static TextField getSearchField() {
        return searchField;
    }

    public static DatePickerContent getPop() {
        return pop;
    }

    public static void setSearchField(TextField searchField) {
        BookingDetails.searchField = searchField;
    }
}
