package sample.Application.DataTypes;

public class Customer {
    public boolean changed;
    int cus_id;
    String name;
    int age;
    String cus_type;
    String phoneNumber;


    public Customer(){

    }
    public Customer(String name, int age, String cus_type,String phoneNr) {
        this.name = name;
        this.age = age;
        this.cus_type = cus_type;
        changed = false;
        this.phoneNumber = phoneNr;

    }

    public Customer(int cus_id, String name, int age, String cus_type, String phoneNr) {
        this.cus_id = cus_id;
        this.name = name;
        this.age = age;
        this.cus_type = cus_type;
        this.changed = false;
        this.phoneNumber = phoneNr;
    }

    public boolean isChanged() {
        return changed;
    }

    public void setChanged(boolean changed) {
        this.changed = changed;
    }

    public int getCus_id() {
        return cus_id;
    }

    public void setCus_id(int cus_id) {
        this.cus_id = cus_id;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getcus_id() {
        return cus_id;
    }

    public void setcus_id(int cus_id) {
        this.cus_id = cus_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCus_type() {
        return cus_type;
    }

    public void setCus_type(String cus_type) {
        this.cus_type = cus_type;
    }


    public String toString(){
        return this.cus_id + " " + this.getName() + " " + this.getAge() + " " + this.getCus_type();
    }
}
