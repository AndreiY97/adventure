package sample.Application.DataTypes;


import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.StringProperty;

public class Reservations {
    IntegerProperty res_id, cus_id, act_id;

    public Reservations() {
        this.res_id = new SimpleIntegerProperty();
        this.cus_id = new SimpleIntegerProperty();
        this.act_id = new SimpleIntegerProperty();
    }

    public int getRes_id() {
        return res_id.get();
    }

    public IntegerProperty res_idProperty() {
        return res_id;
    }

    public void setRes_id(int res_id) {
        this.res_id.set(res_id);
    }

    public int getCus_id() {
        return cus_id.get();
    }

    public IntegerProperty cus_idProperty() {
        return cus_id;
    }

    public void setCus_id(int cus_id) {
        this.cus_id.set(cus_id);
    }

    public int getAct_id() {
        return act_id.get();
    }

    public IntegerProperty act_idProperty() {
        return act_id;
    }

    public void setAct_id(int act_id) {
        this.act_id.set(act_id);
    }
}
