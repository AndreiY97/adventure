package sample.Application.DataTypes.Control;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import sample.Application.DataTypes.Item;
import sample.Database.GetItems;
import sample.Presentation.*;

import java.util.stream.Collectors;

public class ItemsToSellControl {
    private static Button add, edit, back;
    private static ListView<String> list;
    private static ObservableList<String> names;
    private static String itemName;

    public static void initialize() {
        names = FXCollections.observableArrayList();
        list = ItemsToSell.getListView();
        names.addAll(GetItems.getItems().stream().map(Item::getName).collect(Collectors.toList()));
        list.setItems(names);

        back = ItemsToSell.getBackButton();
        back.setOnAction(event -> {
            handleBackButton();
        });

        add = ItemsToSell.getAddButton();
        add.setOnAction(event -> {
            handleAddButton();
        });
        edit = ItemsToSell.getEditButton();
        edit.setOnAction(event -> handleEditButton());
    }
    private static void handleEditButton(){
        itemName = list.getSelectionModel().getSelectedItem();
        System.out.println(itemName+ " == Current Item chosen");

        if(itemName!=null) {
            System.out.println("*BUTTON* [Edit] pressed on ItemsToSell...... -> current scene == ");
            MainControl.getWindow().setScene(EditItemScene.initialize());
            EditItemSceneControl.initialize(itemName);
            System.out.println(itemName+ " == Current Item that will be passed");
            EditItemScene.getLabelEditItem().setText("Edit item: " + itemName);

        }
        else{
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setHeaderText("No item selected!!");
            alert.initOwner(MainControl.getWindow());
            alert.showAndWait();
        }
    }
    private static void handleBackButton() {
        System.out.println("*BUTTON* [Back] pressed on AddItemScene...... -> current scene == ChoiceView");
        MainControl.getWindow().setScene(ChoiceView.initialize());
        ChoiceViewControl.initialize();
    }

    public static void handleAddButton() {
        System.out.println("*BUTTON* [Add] pressed on ItemsToSellScene...... -> current scene == AddItemScene");
        MainControl.getWindow().setScene(AddItemsScene.initialize());
        AddItemsControl.initialize();
    }

    public static String getItemName() {
        return itemName;
    }

    public static void setItemName(String itemName) {
        ItemsToSellControl.itemName = itemName;
    }
}
