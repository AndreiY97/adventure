package sample.Application.DataTypes.Control;

import javafx.stage.Stage;
import sample.Presentation.*;

public class MainControl {

    public static Stage window = new Stage();

    public static Stage getWindow() {
        return window;
    }

    //login scene
    public static void showLoginScene() {

        System.out.println("\nProgram started.... LoginScene displayed\n");

        //initialization of scene
        LoginScene.initialize();
//
//        //initialization of actions
        LoginScene.initialize1();
//
//        //initialize add activity scene
//        AddActivityScene.initialize();
//
//        BookingDetails.initialize();
        //set scene
        window.setScene(LoginScene.getScene());
        window.setTitle("Adventure");


        window.show();
        window.centerOnScreen();
    }

    public static void showOverViewScene() {

        window.setScene(OverViewScene.initialize());
        OverViewSceneControl.initialize();
        window.centerOnScreen();
        window.show();
    }
    public static void showChoiceView() {

        window.setScene(ChoiceView.initialize());
        ChoiceViewControl.initialize();
        window.centerOnScreen();
        window.show();
    }


}
