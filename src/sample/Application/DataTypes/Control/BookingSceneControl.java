package sample.Application.DataTypes.Control;

import javafx.scene.control.Button;
import sample.Application.DataTypes.Customer;
import sample.Presentation.BookingScene;
import sample.Presentation.CompanyScene;
import sample.Presentation.IndividualBooking;
import sample.Presentation.OverViewScene;

public class BookingSceneControl {
    private static Button individual, company, cancel;
    public static Customer customer;

    public static void initialize(){
        System.out.println("\n....BookingScene Scene displayed....\n ");
        customer = new Customer();
        individual = BookingScene.getIndividual();
        individual.setOnAction(e -> {
            handle_individual();
        });
        cancel = BookingScene.getCancel();
        cancel.setOnAction(e -> {
            handle_cancel();
        });

        company = BookingScene.getCompany();
        company.setOnAction(e -> {
            handle_company();
        });

    }
    public static void handle_cancel(){
        System.out.println("*BUTTON* [Cancel] pressed on BookingScene...... -> current scene == OverViewScene");
        MainControl.getWindow().setScene(OverViewScene.initialize());
        OverViewSceneControl.initialize();
    }
    public static void handle_individual(){
        System.out.println("*BUTTON* [Individual] pressed on BookingScene...... -> current scene == IndividualBooking");
        customer.setCus_type("private");
        MainControl.getWindow().setScene(IndividualBooking.initialize());
        IndividualBookingControl.initialize();
    }

    public static void handle_company(){
        System.out.println("*BUTTON* [Company] pressed on BookingScene...... -> current scene == CompanyScene");
        customer.setCus_type("company");
        MainControl.getWindow().setScene(CompanyScene.initialize());
        CompanySceneControl.initialize();
    }
}

