package sample.Application.DataTypes.Control;

import javafx.scene.control.Button;
import sample.Presentation.BookingDetails;
import sample.Presentation.ItemsToSell;
import sample.Presentation.OverViewScene;
import sample.Presentation.ChoiceView;

/**
 * Created by Roxana on 04-Oct-16.
 */
public class ChoiceViewControl {
    private static Button seeBookings, seeActivities, seeItemsToSell;

    public static void initialize(){

        System.out.println("\n....ChoiceView Scene displayed....\n ");
        seeBookings = ChoiceView.getSeeBookings();
        seeBookings.setOnAction(e->handle_seeBookingButton());

        seeActivities = ChoiceView.getSeeActivities();
        seeActivities.setOnAction(e->handle_seeActivitiesButton());

        seeItemsToSell = ChoiceView.getSeeItemsToSell();
        seeItemsToSell.setOnAction(e->handle_seeItemsToSellButton());

    }

    private static void handle_seeItemsToSellButton() {
        System.out.println("*BUTTON* [seeItemsButton] pressed on ChoiceViewScene...... -> current scene == ItemsToSell");
        MainControl.getWindow().setScene(ItemsToSell.initialize());
        ItemsToSellControl.initialize();
    }

    private static void handle_seeBookingButton() {
        System.out.println("*BUTTON* [seeBookingButton] pressed on ChoiceViewScene...... -> current scene == BookingDetails");
        MainControl.getWindow().setScene(BookingDetails.initialize());
        BookingDetailsControl.initialize();
    }
    private static void handle_seeActivitiesButton(){
        System.out.println("*BUTTON* [seeActivitiesButton] pressed on ChoiceViewScene...... -> current scene == OverView");
        MainControl.getWindow().setScene(OverViewScene.initialize());
        OverViewSceneControl.initialize();
    }
}
