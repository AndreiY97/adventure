package sample.Application.DataTypes.Control;

import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import sample.Application.DataTypes.Activities;
import sample.Presentation.AddActivityScene;
import sample.Presentation.Main;
import sample.Presentation.OverViewScene;


public class AddActivityControl {

    private static Button addActivityButton, backButton;
    private static TextField activityField, priceField, maxCapField;


    public static void initialize(){
        System.out.println("\n....AddActivity Scene displayed....\n ");
        activityField = AddActivityScene.getActivityField();
        priceField = AddActivityScene.getPriceField();
        maxCapField = AddActivityScene.getMaxCapacityField();
        addActivityButton = AddActivityScene.getAddActivityButton();
        addActivityButton.setOnAction(e->handle_addAcitivityButton());

        backButton = new AddActivityScene().getBackButton();
        backButton.setOnAction(e->handle_backButton());

    }
    public static void handle_backButton(){
        System.out.println("*BUTTON* [Back] pressed on AddActivity Scene...... -> current scene == OverViewScene");
        MainControl.getWindow().setScene(OverViewScene.initialize());
        OverViewSceneControl.initialize();
    }
    public static void handle_addAcitivityButton(){
        System.out.println("*BUTTON* [addAcitivityButton] pressed on AddActivity Scene...... -> current scene == OverViewScene");
        if(isInputValid()){
            Activities a = new Activities(1, activityField.getText(), Double.parseDouble(priceField.getText()), Integer.parseInt(maxCapField.getText()));
            OverViewScene.getListView().getItems().add(a.getName());
            Main.activities.add(a);
            System.out.println("*OBJECT* [addAcitivityButton] pressed on AddActivity Scene...... -> current scene == OverViewScene\n Added activity == "+ a.getName());
            MainControl.getWindow().setScene(OverViewScene.getScene());
            MainControl.getWindow().show();
        }

    }
    public static boolean isInputValid() {
        String error = "";
        if (activityField.getText() == null||activityField.getText().length()<1)
            error += "Activty name incorrect!\n";

        if (priceField.getText() == null||priceField.getText().length()<1)
            error += "Price incorrect!\n";
        else
            try {
                Double.parseDouble(priceField.getText());
            } catch (NumberFormatException e) {
                error += "Price incorrect!\n";
            }
        if (maxCapField.getText() == null|| maxCapField.getText().length()<1)
            error += "Capacity incorrect!\n";
        else
            try {
                Integer.parseInt(maxCapField.getText());
            } catch (NumberFormatException e) {
                error += "Capacity incorrect!\n";
            }

        if (error.equalsIgnoreCase(""))
            return true;
        else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(MainControl.getWindow());
            alert.setHeaderText("Invalid activity!");
            alert.setContentText(error);
            alert.showAndWait();

            return false;
        }
    }
}
