package sample.Application.DataTypes.Control;

import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import sample.Application.DataTypes.Activities;
import sample.Application.DataTypes.Reserved_act;
import sample.Presentation.BookingScene;
import sample.Presentation.Main;
import sample.Presentation.OverViewScene;
import sample.Presentation.ActivityDetails;

import java.awt.print.Book;
import java.sql.Date;

public class ActivityDetailsControl {

    //fields
    private static java.lang.String nameOfActivity;
    private static Label quantityLabelObs, priceLabelObs;
    private static DatePicker datePicker;
    private static ChoiceBox<Integer> choiceBox;
    private static Button editButton, backButton, continueButton;
    private static TextField priceField, remainingField, maxCapField;
    public static Reserved_act booking;

    //initialize
    public static void initialize(String activityName) {
        System.out.println("\n....ActivityDetails Scene displayed....\n ");
        choiceBox = ActivityDetails.getChoiceBox();
        choiceBox.getItems().addAll(1,2,3,4);
        nameOfActivity = activityName;
        datePicker = ActivityDetails.getDatePicker();
        //textfield
        priceField = ActivityDetails.getPriceField();
        remainingField = ActivityDetails.getRemainingCapacityField();
        maxCapField = ActivityDetails.getMaxCapacityField();

        choiceBox.setOnAction(e-> {
            double price = 0;//Double.parseDouble(priceField.getText());
            for (Activities a : Main.activities)
                if (a.getName().equalsIgnoreCase(nameOfActivity)) {
                    price = a.getPrice();
                }


            price = price*choiceBox.getValue();
            priceField.setText(Double.toString(price));
        });

        for (Activities a : Main.activities) {
            if (a.getName().equalsIgnoreCase(activityName)) {
                priceField.setText(Double.toString(a.getPrice()));
                remainingField.setText(Integer.toString(a.getQuantity()));
                maxCapField.setText(Integer.toString(a.getQuantity()));
            }
        }

        //buttons

        backButton = new ActivityDetails().getBackButton();
        backButton.setOnAction(e -> handle_backButton());

        editButton = ActivityDetails.getEditButton();
        editButton.setOnAction(e -> handle_editButton());

        continueButton = new ActivityDetails().getContinueButton();
        continueButton.setOnAction(e -> handle_book());

    }

    public static void handle_backButton() {
        System.out.println("*BUTTON* [Back] pressed on ActivityDetails Scene...... -> current scene == OverViewScene");
        MainControl.getWindow().setScene(OverViewScene.initialize());
        OverViewSceneControl.initialize();
//        MainControl.getWindow().show();

    }

    public static void handle_book() {
        System.out.println("*BUTTON* [Book] pressed on ActivityDetails Scene...... -> current scene == BookingScene");
        if((datePicker.getValue()!=null)&&(choiceBox.getValue()!=null)){

            int act_id = 0;
            for (Activities a : Main.activities)
                if (a.getName().equalsIgnoreCase(nameOfActivity)) {
                    act_id = a.getAct_id();
                }
            booking = new Reserved_act();
            booking.setAct_id(act_id);
            booking.setRes_date(Date.valueOf(datePicker.getValue()));
            booking.setPayment(Double.parseDouble(priceField.getText()));
            booking.setTime(choiceBox.getValue());
            MainControl.getWindow().setScene(BookingScene.initialize());
            BookingSceneControl.initialize();

        }else
        {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setHeaderText("Invalid input!");
            alert.initOwner(MainControl.getWindow());
            alert.showAndWait();
        }
//        MainControl.getWindow().show();

    }

    public static void handle_editButton() {
        System.out.println("*BUTTON* [Edit] pressed on ActivityDetails Scene...... -> current scene == ActivityDetails");
        if (isInputValid()) {
            for (Activities a : Main.activities) {
                if (a.getName().equalsIgnoreCase(nameOfActivity)) {
                    a.setPrice(Double.parseDouble(priceField.getText()));
                    a.setQuantity(Integer.parseInt(maxCapField.getText()));
                }
            }
        }
        else{
            for(Activities a: Main.activities){
                if(a.getName().equalsIgnoreCase(nameOfActivity)){
                    priceField.setText(Double.toString(a.getPrice()));
                    remainingField.setText(Integer.toString(a.getQuantity()));
                    maxCapField.setText(Integer.toString(a.getQuantity()));
                }
            }
        }
    }

    public static boolean isInputValid() {
        String error = "";

        if (priceField.getText() == null || priceField.getText().length() < 1)
            error += "Price incorrect\n";
        else try {
            Double.parseDouble(priceField.getText());
        } catch (NumberFormatException e) {
            error += "Price incorrect!\n";
        }

        if (maxCapField.getText() == null || maxCapField.getText().length() < 1) {
            error += "Maximum capacity incorrect\n";
        } else try {
            Integer.parseInt(maxCapField.getText());
        } catch (NumberFormatException e) {
            error += "Maximum capacity incorrect!\n";
        }

        if (remainingField.getText() == null || remainingField.getText().length() < 1)
            error += "Remaining capacity incorrect";
        else try {
            Integer.parseInt(remainingField.getText());
        } catch (NumberFormatException e) {
            error += "Remaining capacity incorrect";
        }
        if (error.equalsIgnoreCase(""))
            return true;
        else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setHeaderText("Input incorrect");
            alert.setContentText(error);
            alert.initOwner(MainControl.getWindow());
            alert.showAndWait();
            return false;
        }

    }
}
