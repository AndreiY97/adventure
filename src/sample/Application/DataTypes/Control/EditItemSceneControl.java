package sample.Application.DataTypes.Control;

import javafx.beans.binding.ObjectExpression;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import sample.Application.DataTypes.Activities;
import sample.Application.DataTypes.Item;
import sample.Database.GetItems;
import sample.Database.UpdateItems;
import sample.Presentation.*;

public class EditItemSceneControl {
    private static Button backButton, saveButton;
    private static TextField itemNameField, itemPriceField, itemQuantityField;
    private static ObservableList<Item> updatedList;

    public static void initialize(String itemName){

        itemNameField = EditItemScene.getItemNameTextField();
        itemPriceField = EditItemScene.getPriceTextField();
        itemQuantityField = EditItemScene.getQuantityTextField();

        for (Item m : GetItems.getItems()) {
            if(m.getName().equals(ItemsToSellControl.getItemName())){
                itemNameField.setText(m.getName());
                itemPriceField.setText(String.valueOf(m.getPrice()));
                itemQuantityField.setText(String.valueOf(m.getQuantity()));
            }
        }

        System.out.println("\n....EditItem control displayed....\n ");
        backButton = EditItemScene.getBackButton();
        backButton.setOnAction(e->{
            handle_backButton();
        });
        saveButton = EditItemScene.getSaveButton();
        saveButton.setOnAction(event -> handle_saveButton());

    }
    public static void handle_saveButton(){
        updatedList = FXCollections.observableArrayList();
        System.out.println("*BUTTON* [Edit] pressed on ActivityDetails Scene...... -> current scene == ActivityDetails");
        if (isInputValid()) {
            for (Item m : GetItems.getItems()) {
                if (m.getName().equals(ItemsToSellControl.getItemName())) {
                    m.setName(itemNameField.getText());
                    m.setPrice(Double.parseDouble(itemPriceField.getText()));
                    m.setQuantity(Integer.parseInt(itemQuantityField.getText()));
                    updatedList.add(m);

                }
            }
            UpdateItems.update(updatedList);
            MainControl.getWindow().setScene(ItemsToSell.initialize());
            ItemsToSellControl.initialize();
        }
        else{
            for(Item m: GetItems.getItems()){
                if(m.getName().equals(ItemsToSellControl.getItemName())){
                    itemNameField.setText(m.getName());
                    itemPriceField.setText(String.valueOf(m.getPrice()));
                    itemQuantityField.setText(String.valueOf(m.getQuantity()));
                }
            }
        }
    }
    public static void handle_backButton() {
        System.out.println("*BUTTON* [Back] pressed on EditItemScene...... -> current scene == ItemsToSell");
        MainControl.getWindow().setScene(ItemsToSell.initialize());
        ItemsToSellControl.initialize();
    }


    public static boolean isInputValid() {
        String error = "";

        if (itemPriceField.getText() == null || itemPriceField.getText().length() < 1)
            error += "Price incorrect!\n";
        else try {
            Double.parseDouble(itemPriceField.getText());
        } catch (NumberFormatException e) {
            error += "Price incorrect!\n";
        }

        if (itemQuantityField.getText() == null || itemQuantityField.getText().length() < 1) {
            error += "Maximum capacity incorrect!\n";
        } else try {
            Integer.parseInt(itemQuantityField.getText());
        } catch (NumberFormatException e) {
            error += "Maximum capacity incorrect!\n";
        }

        if (itemNameField.getText() == null || itemNameField.getText().length() < 1)
            error += "Remaining capacity incorrect";
        if (error.equalsIgnoreCase(""))
            return true;
        else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setHeaderText("Input incorrect!");
            alert.setContentText(error);
            alert.initOwner(MainControl.getWindow());
            alert.showAndWait();
            return false;
        }

    }}
