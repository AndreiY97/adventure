package sample.Application.DataTypes.Control;

import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import sample.Application.DataTypes.Activities;
import sample.Presentation.*;

public class OverViewSceneControl {
    private static Button add, book, delete, back;
    private static ListView<String> list;

    public static void initialize(){

        System.out.println("\n....OverView Scene displayed....\n ");

        list = OverViewScene.getListView();

        add = OverViewScene.getAddButton();
        add.setOnAction(event -> {
            handleAddButton();
        });

        book = OverViewScene.getBookButton();
        book.setOnAction(event -> {
            handleBookButton();
        });
        back = OverViewScene.getBackButton();
        back.setOnAction(event -> {
            handleBackButton();
        });

        delete = OverViewScene.getDeleteButton();
        delete.setOnAction(e -> handleDeleteButton());

    }

    private static void handleBackButton() {
        System.out.println("*BUTTON* [Back] pressed on OverViewScene...... -> current scene == ChoiceView");
        MainControl.getWindow().setScene(ChoiceView.initialize());
        ChoiceViewControl.initialize();
    }

    public static void handleAddButton() {
        System.out.println("*BUTTON* [Add] pressed on OverViewScene...... -> current scene == AddActivityScene");
        MainControl.getWindow().setScene(AddActivityScene.initialize());
        AddActivityControl.initialize();


    }

    public static void handleBookButton() {

        String activityName = list.getSelectionModel().getSelectedItem();
        System.out.println(activityName+ " == Current Activity chosen");

        if(activityName!=null) {
            System.out.println("*BUTTON* [Book] pressed on OverViewScene...... -> current scene == AddActivityScene");
            MainControl.getWindow().setScene(ActivityDetails.initialize());
            ActivityDetailsControl.initialize(activityName);
            System.out.println(activityName+ " == Current Activity that will be passed");
            ActivityDetails.getTitle().setText(activityName);

        }
        else{
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setHeaderText("No item selected!!");
            alert.initOwner(MainControl.getWindow());
            alert.showAndWait();
        }
    }

    public static void handleDeleteButton(){
        System.out.println("*BUTTON* [Delete] pressed on OverViewScene...... -> current scene == OverViewScene");
       try {
           String activityName = list.getSelectionModel().getSelectedItem();
           if (activityName != null) {
               list.getItems().remove(activityName);
               for (Activities a : Main.activities) {
                   if (activityName.equalsIgnoreCase(a.getName())) {
                       Main.activities.remove(a);
                       System.out.println("*OBJECT* [Delete] pressed on OverViewScene.... -> "+ a.getName()+" == Deleted");
                   }

               }
           } else {
               Alert alert = new Alert(Alert.AlertType.WARNING);
               alert.setHeaderText("No item selected!!");
               alert.initOwner(MainControl.getWindow());
               alert.showAndWait();
           }
       }catch (Exception e){
           System.out.println("ConcurrentModificationException (no idea what that is but we caught it so we don't get shit)");
       }
    }
}
