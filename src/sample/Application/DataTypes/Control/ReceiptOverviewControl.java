package sample.Application.DataTypes.Control;

import com.sun.org.apache.xerces.internal.impl.dv.xs.DoubleDV;
import com.sun.org.apache.xerces.internal.impl.xpath.XPath;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import sample.Database.BookActivity;
import sample.Presentation.*;


public class ReceiptOverviewControl {
    private static Button bookButton, backButton;

    public static void initialize(){
        if (BookingSceneControl.customer.getCus_type().equals("company")) {
            ReceiptOverview.getCustomerName().setText(CompanyScene.getCompanyNameTextField().getText());
            ReceiptOverview.getNoOfPeopleInfo().setText(CompanyScene.getNoOfPeopleTextField().getText());
            ReceiptOverview.getPriceInfo().setText(ActivityDetails.getPriceField().getText());
            ReceiptOverview.getTimeInfo().setText(String.valueOf(ActivityDetails.getChoiceBox().getValue()));
            double totalPrice = (Double.parseDouble(ActivityDetails.getPriceField().getText()) * Double.parseDouble(CompanyScene.getNoOfPeopleTextField().getText()) * 0.8) + Sell_ItemSceneControl.getP();
            ReceiptOverview.getTotalPrice().setText(String.valueOf(totalPrice));
        }else {
            ReceiptOverview.getCustomerName().setText(IndividualBooking.getFirstNameField().getText() + " " + IndividualBooking.getLastNameField().getText());
            ReceiptOverview.getNoOfPeopleInfo().setText(IndividualBooking.getNoOfPeopleField().getText());
            ReceiptOverview.getPriceInfo().setText(ActivityDetails.getPriceField().getText());
            ReceiptOverview.getTimeInfo().setText(String.valueOf(ActivityDetails.getChoiceBox().getValue()));
            double tPrice = (Double.parseDouble(IndividualBooking.getNoOfPeopleField().getText()) * Double.parseDouble(ActivityDetails.getPriceField().getText())) + Sell_ItemSceneControl.getP();
            ReceiptOverview.getTotalPrice().setText(String.valueOf(tPrice));
        }

        System.out.println("\n....ReceiptOverview Scene displayed....\n ");
        backButton = ReceiptOverview.getBackButton();
        backButton.setOnAction(e->{
            handle_backButton();
        });

        bookButton = ReceiptOverview.getBookButton();
        bookButton.setOnAction(e->{
            handle_bookButton();
        });
    }
    public static void handle_backButton() {
        System.out.println("*BUTTON* [Back] pressed on ReceiptOverview...... -> current scene == IndividualBooking");
        MainControl.getWindow().setScene(Sell_ItemScene.initialize());
        Sell_ItemSceneControl.initialize();
    }

    public static void handle_bookButton() {
        ActivityDetailsControl.booking.setPayment(Double.parseDouble(ReceiptOverview.getTotalPrice().getText()));
        BookActivity.addReservation(ActivityDetailsControl.booking, BookingSceneControl.customer);
        MainControl.getWindow().setScene(ChoiceView.initialize());
        ChoiceViewControl.initialize();
    }
}
