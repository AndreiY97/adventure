package sample.Application.DataTypes.Control;

import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextField;
import sample.Application.DataTypes.Item;
import sample.Database.AddItem;
import sample.Database.GetItems;
import sample.Presentation.AddActivityScene;
import sample.Presentation.AddItemsScene;
import sample.Presentation.ItemsToSell;
import sample.Presentation.OverViewScene;

public class AddItemsControl {
    private static Button addItemButton, backButton;
    private static TextField nameField, priceField, maxCapField;


    public static void initialize(){
        System.out.println("\n....AddItem Scene displayed....\n ");
        nameField = AddItemsScene.getNameField();
        priceField = AddItemsScene.getPriceField();
        maxCapField = AddItemsScene.getMaxCapacityField();

        addItemButton = AddItemsScene.getAddItemButton();
        addItemButton.setOnAction(e->handle_addItemButton());

        backButton = new AddItemsScene().getBackButton();
        backButton.setOnAction(e->handle_backButton());

    }

    private static void handle_addItemButton() {

        if (isInputValid()){
            Item m = new Item(nameField.getText(), Double.parseDouble(priceField.getText()),Integer.parseInt(maxCapField.getText()));
            if(checkForDuplicates(m)){
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.initOwner(MainControl.getWindow());
                alert.setHeaderText("Duplicate found!");
                alert.setContentText("A duplicate is found!");
                alert.showAndWait();
            }else {
                AddItem.addItem(m);
                MainControl.getWindow().setScene(ItemsToSell.initialize());
                ItemsToSellControl.initialize();
            }
        }
    }
    private static boolean checkForDuplicates (Item itm){
        for (Item m : GetItems.getItems()) {
            if (m.getName().equals(itm.getName())) return true;
        }
        return false;
    }
    public static void handle_backButton(){
        System.out.println("*BUTTON* [Back] pressed on AddItem Scene...... -> current scene == ItemsToSellScene");
        MainControl.getWindow().setScene(ItemsToSell.initialize());
        ItemsToSellControl.initialize();
    }
    public static boolean isInputValid() {
        String error = "";
        if (nameField.getText() == null||nameField.getText().length()<1)
            error += "Item name incorrect!\n";

        if (priceField.getText() == null||priceField.getText().length()<1)
            error += "Price is incorrect!\n";
        else
            try {
                Double.parseDouble(priceField.getText());
            } catch (NumberFormatException e) {
                error += "Price is incorrect!\n";
            }
        if (maxCapField.getText() == null|| maxCapField.getText().length()<1)
            error += "Quantity incorrect!\n";
        else
            try {
                Integer.parseInt(maxCapField.getText());
            } catch (NumberFormatException e) {
                error += "Quantity incorrect!\n";
            }

        if (error.equalsIgnoreCase(""))
            return true;
        else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(MainControl.getWindow());
            alert.setHeaderText("Invalid item!");
            alert.setContentText(error);
            alert.showAndWait();

            return false;
        }
    }
}
