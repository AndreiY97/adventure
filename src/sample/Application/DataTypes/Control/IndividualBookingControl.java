package sample.Application.DataTypes.Control;

import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import sample.Presentation.*;

public class IndividualBookingControl {
    private static Button nextButton, backButton;
    private static TextField firstNameField, lastNameField, ageField, phoneField, noOfPeopleField;

    public static void initialize(){
        System.out.println("\n....IndividualBooking Scene displayed....\n ");

        firstNameField = IndividualBooking.getFirstNameField();
        lastNameField = IndividualBooking.getLastNameField();
        ageField = IndividualBooking.getAgeField();
        phoneField = IndividualBooking.getPhoneField();
        noOfPeopleField = IndividualBooking.getNoOfPeopleField();

        backButton = IndividualBooking.getBackButton();
        backButton.setOnAction(e->{
            handle_backButton();
        });
        nextButton = IndividualBooking.getNextButton();
        nextButton.setOnAction(e->handle_nextButton());

    }
    public static void handle_nextButton(){

            if (isInputValid()) {
                BookingSceneControl.customer.setAge(Integer.parseInt(ageField.getText()));
                BookingSceneControl.customer.setName(firstNameField.getText() + "_" + lastNameField.getText());
                BookingSceneControl.customer.setPhoneNumber(phoneField.getText());
                BookingSceneControl.customer.setAge(Integer.parseInt(ageField.getText()));
                ActivityDetailsControl.booking.setAge_of_youngest(Integer.parseInt(ageField.getText()));
                ActivityDetailsControl.booking.setNumber_of_people(Integer.parseInt(noOfPeopleField.getText()));

                MainControl.getWindow().setScene(Sell_ItemScene.initialize());
                Sell_ItemSceneControl.initialize();
            }
    }
    public static void handle_backButton(){
        System.out.println("*BUTTON* [Back] pressed on IndividualBooking Scene...... -> current scene == BookingScene");
        MainControl.getWindow().setScene(BookingScene.initialize());
        BookingSceneControl.initialize();

    }
    public static boolean isInputValid(){
        String error = "";
        if(firstNameField==null)
            error+="Invalid first name!\n";
        if(lastNameField.getText().isEmpty())
            error+="Invalid last name!\n";
        int age = Integer.parseInt(IndividualBooking.getAgeField().getText());
        String activity = String.valueOf(OverViewScene.getListView().getSelectionModel().getSelectedItems());
        if (age < 10 && activity.equalsIgnoreCase("[Go Cart]"))
            error+="Not allowed age!\n";
        if(ageField.getText().isEmpty())
            error+="Invalid age!\n";
        else try{
            Integer.parseInt(ageField.getText());

        }catch(NumberFormatException e){
            error+="Invalid age!\n";
        }
        if(phoneField.getText().isEmpty())
            error+="Invalid phone nr!\n";

        if(noOfPeopleField.getText().isEmpty())
            error+="Invalid number of people!\n";
        else try{
            Integer.parseInt(noOfPeopleField.getText());
        }catch (Exception e){
            error+="Invalid number of people!\n";
        }
        if(error.equalsIgnoreCase(""))
            return true;
        else{
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setHeaderText("Invalid input");
            alert.setContentText(error);
            alert.initOwner(MainControl.getWindow());
            alert.showAndWait();
        }
        return false;
    }
}
