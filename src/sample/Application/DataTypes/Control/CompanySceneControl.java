package sample.Application.DataTypes.Control;

import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import sample.Presentation.*;

public class CompanySceneControl {
    private static Button nextButton, backButton;
    public static TextField companyNameTextField, ageOfYoungestTextField, phoneNrTextField, noOfPeopleTextField;
    public static void initialize() {

        System.out.println("\n....CompanyScene displayed....\n ");
        companyNameTextField = CompanyScene.getCompanyNameTextField();
        ageOfYoungestTextField =CompanyScene.getAgeOfYoungestTextField();
        phoneNrTextField = CompanyScene.getPhoneNrTextField();
        noOfPeopleTextField = CompanyScene.getNoOfPeopleTextField();

        nextButton = CompanyScene.getNextButton();
        nextButton.setOnAction(e -> handle_nextButton());

        backButton = CompanyScene.getBackButton();
        backButton.setOnAction(e -> handle_backButton());
    }

    public static void handle_nextButton() {

           if (isInputValid()) {
               BookingSceneControl.customer.setAge(Integer.parseInt(ageOfYoungestTextField.getText()));
               BookingSceneControl.customer.setName(companyNameTextField.getText());
               BookingSceneControl.customer.setPhoneNumber(phoneNrTextField.getText());
               BookingSceneControl.customer.setAge(Integer.parseInt(ageOfYoungestTextField.getText()));
               ActivityDetailsControl.booking.setAge_of_youngest(Integer.parseInt(ageOfYoungestTextField.getText()));
               ActivityDetailsControl.booking.setNumber_of_people(Integer.parseInt(noOfPeopleTextField.getText()));

               MainControl.getWindow().setScene(Sell_ItemScene.initialize());
               Sell_ItemSceneControl.initialize();
           }
    }

    public static void handle_backButton(){
        System.out.println("*BUTTON* [Back] pressed on CompanyScene...... -> current scene == BookingScene");
        MainControl.getWindow().setScene(BookingScene.initialize());
        BookingSceneControl.initialize();
    }
    public static boolean isInputValid(){
        String error = "";
        if(companyNameTextField==null)
            error+="Invalid first name!\n";
        if(companyNameTextField.getText().isEmpty())
            error+="Invalid last name!\n";
        int age = Integer.parseInt(CompanyScene.getAgeOfYoungestTextField().getText());
        String activity = String.valueOf(OverViewScene.getListView().getSelectionModel().getSelectedItems());
        if (age < 10 && activity.equalsIgnoreCase("[Go Cart]"))
            error+="Not allowed age!\n";
        if(ageOfYoungestTextField.getText().isEmpty())
            error+="Invalid age!\n";
        else try{
            Integer.parseInt(ageOfYoungestTextField.getText());

        }catch(NumberFormatException e){
            error+="Invalid age!\n";
        }
        if(phoneNrTextField.getText().isEmpty())
            error+="Invalid phone nr!\n";

        if(noOfPeopleTextField.getText().isEmpty())
            error+="Invalid number of people!\n";
        else try{
            Integer.parseInt(noOfPeopleTextField.getText());
        }catch (Exception e){
            error+="Invalid number of people!\n";
        }
        if(error.equalsIgnoreCase(""))
            return true;
        else{
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setHeaderText("Invalid input");
            alert.setContentText(error);
            alert.initOwner(MainControl.getWindow());
            alert.showAndWait();
        }
        return false;
    }
}


