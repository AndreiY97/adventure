package sample.Application.DataTypes.Control;

import com.sun.javafx.scene.control.skin.DatePickerContent;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import sample.Application.DataTypes.BookingsTable;
import sample.Application.DataTypes.Reservations;
import sample.Database.FetchReservations;
import sample.Presentation.BookingDetails;
import sample.Presentation.ChoiceView;
import sample.Presentation.EditBookingScene;
import sample.Presentation.Main;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;


public class BookingDetailsControl {
    private static Button backButton, editButton, cancelButton, checkButton, resetButton;
    private static TextField search;
    private static TableView<BookingsTable> table;
    private static ObservableList<BookingsTable> bookings, tableItems;
    private static DatePickerContent pop;
    private static ObservableList<BookingsTable> filteredByDateBookings;
    private static LocalDate selectedDate;

    public static void initialize(){
        filteredByDateBookings = FXCollections.observableArrayList();
        pop = BookingDetails.getPop();
        //table
        table = BookingDetails.getBookingDetails();
        table.setItems(FetchReservations.getBookingTableItems());
        //table.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> displayBookingInfo(newValue));

        System.out.println("\n....BookingDetails Scene displayed....\n ");
        backButton = BookingDetails.getBackButton();
        backButton.setOnAction(e-> handle_backButton());

        editButton = BookingDetails.getEditButton();
        editButton.setOnAction(event -> handle_editButton());

        cancelButton = BookingDetails.getCancelButton();
        cancelButton.setOnAction(event -> handle_cancelButton());

        search = BookingDetails.getSearchField();
        bookings = table.getItems();
        initializeSearch();

        checkButton = BookingDetails.getCheckButton();
        checkButton.setOnAction(event -> handle_check_button());
        resetButton = BookingDetails.getResetButton();
        resetButton.setOnAction(event -> handle_reset_button());
    }
    public static void handle_reset_button(){
        table.setItems(FetchReservations.getBookingTableItems());
    }
    public static void handle_check_button(){
        filteredByDateBookings.clear();
        List<DateCell> dateCellList = getAllDateCells(pop);
        for (DateCell cell : dateCellList) {
            cell.addEventHandler(
                    MouseEvent.MOUSE_PRESSED,(e)-> {
                        selectedDate = cell.getItem();

                    }
            );
        }
        filteredByDateBookings.addAll(FetchReservations.filterByDate(selectedDate));
        table.setItems(filteredByDateBookings);

    }
    public static void initializeSearch(){
        search.textProperty().addListener(new InvalidationListener() {
            @Override
            public void invalidated(Observable observable) {
                if (search.textProperty().get().isEmpty()) {
                    table.setItems(bookings);
                    return;
                }

                tableItems = FXCollections.observableArrayList();

                for(BookingsTable b : bookings){
                    if(b.getPhoneDB().toUpperCase().contains(search.getText().toUpperCase())||
                            b.getCustomerNameDB().toUpperCase().contains(search.getText().toUpperCase())) {

                        tableItems.add(b);
                }
                }

                table.setItems(tableItems);
            }
        });
    }
    public static void handle_cancelButton(){
        System.out.println("*BUTTON* [Cancel] pressed on BookingDetails Scene...... -> current scene == ChoiceView");
        BookingsTable bookingTable = table.getSelectionModel().getSelectedItem();
        Reservations booking = new Reservations();

        if (bookingTable != null) {
            for (Reservations b : FetchReservations.getBookings())
                if (bookingTable.getBookingId() == b.getRes_id()) {
                    booking = b;
                    break;
                }
        }
        cancelBooking(bookingTable,booking);
    }
    public static void cancelBooking(BookingsTable bookingTable, Reservations booking) {

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.initOwner(MainControl.getWindow());
        alert.setHeaderText("Remove booking");
        alert.setContentText("Are you sure you want to cancel?");

        Optional<ButtonType> result = alert.showAndWait();

        if (result.get() == ButtonType.OK) { //confirmed

            FetchReservations.deleteBooking(booking); //delete booking from database

            table.setItems(FetchReservations.getBookingTableItems()); //set table items
            bookings = table.getItems();

            Alert alert1 = new Alert(Alert.AlertType.CONFIRMATION);
            alert1.initOwner(MainControl.getWindow());
            alert1.setContentText("Booking canceled!\n");
            alert1.showAndWait();
            System.out.println("a booking removed");
        }
        else {
            alert.close();
        }
    }
    public static void handle_backButton(){
        System.out.println("*BUTTON* [Back] pressed on BookingDetails Scene...... -> current scene == ChoiceView");
        MainControl.getWindow().setScene(ChoiceView.initialize());
        ChoiceViewControl.initialize();
    }

    public static void handle_editButton() {
        System.out.println("*BUTTON* [Edit] pressed on BookingDetails Scene...... -> current scene == EditBooking");
        MainControl.getWindow().setScene(EditBookingScene.initialize());
        EditBookingSceneControl.initialize();
    }
    private static List<DateCell> getAllDateCells(DatePickerContent content)
    {
        List<DateCell> result = new ArrayList<>();

        for (Node n : content.getChildren())
        {
            System.out.println("node " + n + n.getClass());
            if (n instanceof GridPane)
            {
                GridPane grid = (GridPane) n;
                for (Node gChild : grid.getChildren())
                {
                    System.out.println("grid node: " + gChild + gChild.getClass());
                    if (gChild instanceof DateCell)
                    {
                        result.add((DateCell) gChild);
                    }
                }
            }
        }

        return result;
    }
}
