package sample.Application.DataTypes.Control;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import sample.Application.DataTypes.Activities;
import sample.Application.DataTypes.Item;
import sample.Database.GetItems;
import sample.Presentation.*;

import javax.xml.crypto.dom.DOMCryptoContext;
import java.util.stream.Collectors;


public class Sell_ItemSceneControl {
    private static Button backButton, addItemButton, skipButton;
    private static TextField priceField, quantityField;
    private static ChoiceBox<String> choiceBox;
    private static ObservableList<String> names;
    private static double price, p;
    private static double priceT, quantity;
    public static void initialize() {
        priceField = Sell_ItemScene.getPriceField();
        priceField.setDisable(true);
        choiceBox = Sell_ItemScene.getChoiceBox();
        names = FXCollections.observableArrayList();
        names.addAll(GetItems.getItems().stream().map(Item::getName).collect(Collectors.toList()));
        choiceBox.setItems(names);

        choiceBox.setOnAction(e-> handleChoiceBox());

        quantityField = Sell_ItemScene.getQuantityField();
        quantityField.textProperty().addListener((observable, oldValue, newValue) ->{
            if (!newValue.equals("")){
                priceField.setText("");
                quantity = Double.parseDouble(quantityField.getText());
                priceT = priceT * (Double.parseDouble(quantityField.getText()));
                priceField.setText(Double.toString(priceT));
                priceField.setDisable(true);
                handleChoiceBox();
            }
        });

        backButton = Sell_ItemScene.getBackButton();
        backButton.setOnAction(event -> handleBackButton());

        addItemButton = Sell_ItemScene.getAddItemButton();
        addItemButton.setOnAction(event -> handleAddItemButton());

        skipButton = Sell_ItemScene.getSkipButton();
        skipButton.setOnAction(event -> handleSkipButton());



    }
    private static double handleChoiceBox(){
        priceT = 0;//Double.parseDouble(priceField.getText());
        for (Item itm : GetItems.getItems())
            if (itm.getName().equalsIgnoreCase(choiceBox.getValue())) {
                priceT = itm.getPrice();
            }
        return priceT;
    }

    private static void handleAddItemButton() {
        if (isInputValid()){
            System.out.println("Add Item button pressed");
            price = Double.parseDouble(priceField.getText());
            choiceBox.getSelectionModel().clearSelection();
            priceField.setText("");
            quantityField.setText("");
            p = p + price;
            price = 0;
        }
    }

    private static void handleSkipButton() {
        MainControl.getWindow().setScene(ReceiptOverview.initialize());
        ReceiptOverviewControl.initialize();
    }

    private static void handleBackButton() {
        if (BookingSceneControl.customer.getCus_type().equals("company")) {
            MainControl.getWindow().setScene(CompanyScene.initialize());
            CompanySceneControl.initialize();
            System.out.println("Back Button Pressed From Company");
        } else {
            MainControl.getWindow().setScene(IndividualBooking.initialize());
            IndividualBookingControl.initialize();
            System.out.println("Back Button Pressed From Individual");
        }
    }

    public static double getPrice() {
        return price;
    }

    public static void setPrice(double price) {
        Sell_ItemSceneControl.price = price;
    }

    public static double getP() {
        return p;
    }

    public static boolean isInputValid() {
        String error = "";

        if (priceField.getText() == null || priceField.getText().length() < 1)
            error += "Price is not valid!\n";
        else try {
            Double.parseDouble(priceField.getText());
        } catch (NumberFormatException e) {
            error += "Price incorrect!\n";
        }

        if (quantityField.getText() == null || quantityField.getText().length() < 1) {
            error += "Quantity incorrect!\n";
        } else try {
            Integer.parseInt(quantityField.getText());
        } catch (NumberFormatException e) {
            error += "Quantity incorrect!\n";
        }
        if (error.equalsIgnoreCase(""))
            return true;
        else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setHeaderText("Incorrect Input!");
            alert.setContentText(error);
            alert.initOwner(MainControl.getWindow());
            alert.showAndWait();
            return false;
        }

    }
}