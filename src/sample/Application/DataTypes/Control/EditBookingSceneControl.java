package sample.Application.DataTypes.Control;

import javafx.scene.control.Button;
import sample.Presentation.BookingDetails;
import sample.Presentation.EditBookingScene;

public class EditBookingSceneControl {
    private static Button backButton;

    public static void initialize(){
        System.out.println("\n....EditBooking control displayed....\n ");
        backButton = EditBookingScene.getBackButton();
        backButton.setOnAction(e->{
            handle_backButton();
        });
    }

    public static void handle_backButton() {
        System.out.println("*BUTTON* [Back] pressed on EditBookingScene...... -> current scene == BookingDetails");
        MainControl.getWindow().setScene(BookingDetails.initialize());
        BookingDetailsControl.initialize();
    }
}
