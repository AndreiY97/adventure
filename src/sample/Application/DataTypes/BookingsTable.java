package sample.Application.DataTypes;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by theod on 08-Oct-16.
 */
public class BookingsTable {
    IntegerProperty bookingId;
    StringProperty customerNameDB, activityNameDB, timeDB, dateDB, customerTypeDB, phoneDB, paymentDB, ageDB, numberOfPeople;

    public BookingsTable() {
        this.bookingId = new SimpleIntegerProperty();
        this.customerNameDB = new SimpleStringProperty("");
        this.activityNameDB = new SimpleStringProperty("");
        this.timeDB = new SimpleStringProperty("");
        this.dateDB = new SimpleStringProperty("");
        this.customerTypeDB = new SimpleStringProperty("");
        this.phoneDB = new SimpleStringProperty("");
        this.paymentDB = new SimpleStringProperty("");
        this.ageDB = new SimpleStringProperty("");
        this.numberOfPeople = new SimpleStringProperty("");
    }

    public int getBookingId() {
        return bookingId.get();
    }

    public IntegerProperty bookingIdProperty() {
        return bookingId;
    }

    public void setBookingId(int bookingId) {
        this.bookingId.set(bookingId);
    }

    public String getCustomerNameDB() {
        return customerNameDB.get();
    }

    public StringProperty customerNameDBProperty() {
        return customerNameDB;
    }

    public void setCustomerNameDB(String customerNameDB) {
        this.customerNameDB.set(customerNameDB);
    }

    public String getActivityNameDB() {
        return activityNameDB.get();
    }

    public StringProperty activityNameDBProperty() {
        return activityNameDB;
    }

    public void setActivityNameDB(String activityNameDB) {
        this.activityNameDB.set(activityNameDB);
    }

    public String getTimeDB() {
        return timeDB.get();
    }

    public StringProperty timeDBProperty() {
        return timeDB;
    }

    public void setTimeDB(String timeDB) {
        this.timeDB.set(timeDB);
    }

    public String getDateDB() {
        return dateDB.get();
    }

    public StringProperty dateDBProperty() {
        return dateDB;
    }

    public void setDateDB(String dateDB) {
        this.dateDB.set(dateDB);
    }

    public String getCustomerTypeDB() {
        return customerTypeDB.get();
    }

    public StringProperty customerTypeDBProperty() {
        return customerTypeDB;
    }

    public void setCustomerTypeDB(String customerTypeDB) {
        this.customerTypeDB.set(customerTypeDB);
    }

    public String getPhoneDB() {
        return phoneDB.get();
    }

    public StringProperty phoneDBProperty() {
        return phoneDB;
    }

    public void setPhoneDB(String phoneDB) {
        this.phoneDB.set(phoneDB);
    }

    public String getPaymentDB() {
        return paymentDB.get();
    }

    public StringProperty paymentDBProperty() {
        return paymentDB;
    }

    public void setPaymentDB(String paymentDB) {
        this.paymentDB.set(paymentDB);
    }

    public String getAgeDB() {
        return ageDB.get();
    }

    public StringProperty ageDBProperty() {
        return ageDB;
    }

    public void setAgeDB(String ageDB) {
        this.ageDB.set(ageDB);
    }

    public String getNumberOfPeople() {
        return numberOfPeople.get();
    }

    public StringProperty numberOfPeopleProperty() {
        return numberOfPeople;
    }

    public void setNumberOfPeople(String numberOfPeople) {
        this.numberOfPeople.set(numberOfPeople);
    }
}
