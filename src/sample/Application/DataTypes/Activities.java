package sample.Application.DataTypes;

public class Activities {

    int act_id;
    String name;
    double price;
    //am adaugat asta, semnat Tudorisimo
    //am vazut :)  semnat andrei
    int quantity;

    public Activities(int act_id, String name, double price, int quantity ) {
        this.act_id = act_id;
        this.name = name;
        this.price = price;
        this.quantity = quantity;

    }

    public int getQuantity(){return quantity; }

    public int getAct_id() { return act_id; }

    public void setAct_id(int act_id) {
        this.act_id = act_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setQuantity(int quantity){ this.quantity = quantity; }

    public String toString(){
        return act_id + " " + name + " " + price + " " + quantity;
    }

}

