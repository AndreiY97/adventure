package sample.Application.DataTypes;

public class Item {

    public boolean changed;
    int item_id;
    String name;
    double price;
    int quantity;

    public Item (){

    }

    public Item(int item_id, String name, double price, int quantity) {
        this.item_id = item_id;
        this.name = name;
        this.price = price;
        this.quantity = quantity;
        changed = false;
    }
    public Item(String name, double price, int quantity) {
        this.item_id = item_id;
        this.name = name;
        this.price = price;
        this.quantity = quantity;
        changed = false;
    }

    public Item(String name, double price, int quantity, boolean changed) {
        this.name = name;
        this.price = price;
        this.quantity = quantity;
        this.changed = changed;
    }

    public int getItem_id() {
        return item_id;
    }

    public void setItem_id(int id) {
        this.item_id = item_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }


    public String toString(){
        return this.getItem_id() + " " + this.getName() + " " + this.getPrice();
    }
}
