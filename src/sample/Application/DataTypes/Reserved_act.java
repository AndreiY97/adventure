package sample.Application.DataTypes;

import java.util.Date;

public class Reserved_act {

    public boolean changed;
    int res_id=0;
    int cus_id=0;
    int act_id=0;
    double payment;
    int number_of_people;
    int age_of_youngest;
    Date res_date;
    int time;


    public Reserved_act(){

    }


    public Reserved_act(int res_id, int cus_id, int act_id, double payment, int number_of_people, int age_of_youngest, Date res_date, int time) {
        this.res_id = res_id;
        this.cus_id = cus_id;
        this.act_id = act_id;
        this.payment = payment;
        this.number_of_people = number_of_people;
        this.age_of_youngest = age_of_youngest;
        this.res_date = res_date;
        this.time = time;
        this.changed = false;
    }

    public Reserved_act(int act_id, double payment, int number_of_people, int age_of_youngest, Date res_date, int time, boolean changed) {
        this.act_id = act_id;
        this.payment = payment;
        this.number_of_people = number_of_people;
        this.age_of_youngest = age_of_youngest;
        this.res_date = res_date;
        this.time = time;
        this.changed = changed;
    }

    public boolean isChanged() {
        return changed;
    }

    public void setChanged(boolean changed) {
        this.changed = changed;
    }

    public double getPayment() {
        return payment;
    }

    public void setPayment(double payment) {
        this.payment = payment;
    }

    public int getNumber_of_people() {
        return number_of_people;
    }

    public void setNumber_of_people(int number_of_people) {
        this.number_of_people = number_of_people;
    }

    public int getAge_of_youngest() {
        return age_of_youngest;
    }

    public void setAge_of_youngest(int age_of_youngest) {
        this.age_of_youngest = age_of_youngest;
    }

    public Date getRes_date() {
        return res_date;
    }

    public void setRes_date(Date res_date) {
        this.res_date = res_date;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }


    public int getRes_id() {
        return res_id;
    }

    public void setRes_id(int res_id) {
        this.res_id = res_id;
    }

    public int getCus_id() {
        return cus_id;
    }

    public void setCus_id(int cus_id) {
        this.cus_id = cus_id;
    }

    public int getAct_id() {
        return act_id;
    }

    public void setAct_id(int act_id) {
        this.act_id = act_id;
    }
}
