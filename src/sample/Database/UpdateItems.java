package sample.Database;

import javafx.collections.ObservableList;
import sample.Application.DataTypes.Customer;
import sample.Application.DataTypes.Item;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Created by andreiungureanu on 07/10/2016.
 */
public class UpdateItems {


    static final String DB_URL = "jdbc:mysql://localhost:3306/keadb?useSSL=false";

    //  Database credentials
    static final String USER = "root";
    static final String PASS = "root";


    public static void update(ObservableList<Item> items)

    {
        Connection conn = null;
        Statement stmt = null;

        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();
            for (Item i: items){
                //if(i.changed){
                    String sql = "UPDATE ITEMS_RECORD SET PRODUCT_NAME = '" + i.getName() + "', PRODUCT_PRICE = '" + i.getPrice() + "', PRODUCT_CAPACITY = '" + i.getQuantity()+ "' WHERE PRODUCT_ID = '" + i.getItem_id() + "';";
                    stmt.execute(sql);
                //}
            }
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }

        }

    }

}
