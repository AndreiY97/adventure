package sample.Database;

import sample.Application.DataTypes.Customer;
import sample.Application.DataTypes.Reserved_act;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by andreiungureanu on 04/10/2016.
 */
public class FetchCustomers {

    static final String DB_URL = "jdbc:mysql://localhost:3306/keadb?useSSL=false";

    //  Database credentials
    static final String USER = "root";
    static final String PASS = "root";

    public static ArrayList<Customer> getCustomers(){
        ArrayList<Customer> customers = new ArrayList();
        Connection conn = null;
        Statement stmt = null;
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();
            String sql;
            sql = "select * from CUSTOMERS;";
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()){
                customers.add(new Customer(rs.getInt("CUS_ID"),rs.getString("NAME"),rs.getInt("AGE"),rs.getString("CUS_TYPE"),rs.getString("PHONE")));
            }
            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
            return customers;
        }

    }

}
