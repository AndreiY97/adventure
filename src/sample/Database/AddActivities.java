package sample.Database;

import sample.Application.DataTypes.Activities;
import sample.Application.DataTypes.Customer;
import sample.Application.DataTypes.Reserved_act;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by andreiungureanu on 06/10/2016.
 */
public class AddActivities {

    static final String DB_URL = "jdbc:mysql://localhost:3306/keadb?useSSL=false";

    //  Database credentials
    static final String USER = "root";
    static final String PASS = "root";


    public static void addActvities(ArrayList<Activities> activities)

    {
        Connection conn = null;
        Statement stmt = null;
        Statement stmt2 = null;
        int CUS_ID = -1;
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();
            String sql;
            String sql2;
            for (Activities a : activities) {
                int index = -1;
                sql = "SELECT ACT_ID FROM ACTIVITIES WHERE ACT_NAME = '" + a.getName() + "';";
                ResultSet rs = stmt.executeQuery(sql);
                while(rs.next()){
                    index = rs.getInt("ACT_ID");
                }
                if(index!=-1){
                    sql2 = "INSERT INTO ACTIVITIES (ACT_NAME,PRICE,CAPACITY) VALUES ('" + a.getName() + "'," + a.getPrice() + "," + a.getQuantity() + ");";
                    stmt2.execute(sql2);
                    sql2 = "";
                }
            }

            stmt2.close();
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }

        }
    }
}
