package sample.Database;

import sample.Application.DataTypes.Activities;
import sample.Application.DataTypes.Customer;
import sample.Application.DataTypes.Reserved_act;

import java.sql.*;
import java.util.*;
import java.util.concurrent.SynchronousQueue;

/**
 * Created by andreiungureanu on 04/10/2016.
 */
public class BookActivity {

    static final String DB_URL = "jdbc:mysql://localhost:3306/keadb?useSSL=false";

    //  Database credentials
    static final String USER = "root";
    static final String PASS = "root";


    public static void addReservation(Reserved_act act, Customer cus)

    {
        Connection conn = null;
        Statement stmt = null;
        Statement stmt1 = null;
        Statement stmt2 = null;
        int CUS_ID = -1;
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();
            stmt1 = conn.createStatement();
            stmt2 = conn.createStatement();
            String sql;
            String sql1;
            String sql2;
            sql = "INSERT INTO CUSTOMERS(NAME, AGE, CUS_TYPE, PHONE) VALUES ('" + cus.getName() + "','" + cus.getAge() + "','" + cus.getCus_type() + "','" + cus.getPhoneNumber() + "');";
            stmt.execute(sql);
            sql2 = "SELECT * FROM CUSTOMERS WHERE NAME = " + "'" + cus.getName() + "' AND PHONE = '" + cus.getPhoneNumber() +"';" ;
            ResultSet rs = stmt2.executeQuery(sql2);
            while (rs.next()){
                CUS_ID = rs.getInt("CUS_ID");
            }
            sql1 = "INSERT INTO RESERVED_ACT(CUS_ID,ACT_ID,PAYMENT,NUMBER_OF_PEOPLE,AGE_OF_YOUNGEST,RES_DATE,TIME) VALUES ('" + CUS_ID + "','" + act.getAct_id() + "','" + act.getPayment() + "','" + act.getNumber_of_people() + "','" + act.getAge_of_youngest() + "','" + act.getRes_date() + "','" + act.getTime() + "')";
            if(CUS_ID == -1){
                stmt.execute(sql);
                rs = stmt2.executeQuery(sql2);
                while (rs.next()){
                    CUS_ID = rs.getInt("CUS_ID");
                }
            }
            stmt1.execute(sql1);
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }

        }

    }


}
