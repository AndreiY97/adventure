package sample.Database;

import sample.Application.DataTypes.Activities;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by andreiungureanu on 01/10/2016.
 */
public class FetchActivites {

    static final String DB_URL = "jdbc:mysql://localhost:3306/keadb?useSSL=false";

    //  Database credentials
    static final String USER = "root";
    static final String PASS = "root";


    public static ArrayList<Activities> getActivites() {
        Connection conn = null;
        Statement stmt = null;
        ArrayList<Activities> activities = new ArrayList<>();
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();
            String sql;
            sql = "SELECT * FROM ACTIVITIES;";
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()){
                activities.add(new Activities(rs.getInt(1),rs.getString(2),rs.getDouble(3),rs.getInt(4)));
            }
            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
            return activities;
        }

    }
}



