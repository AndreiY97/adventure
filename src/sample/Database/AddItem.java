package sample.Database;

import sample.Application.DataTypes.Item;
import sample.Application.DataTypes.Reserved_act;

import java.sql.*;

public class AddItem {

    static final String DB_URL = "jdbc:mysql://localhost:3306/keadb?useSSL=false";

    //  Database credentials
    static final String USER = "root";
    static final String PASS = "root";


    public static void addItem(Item item){

        Connection conn = null;
        Statement stmt = null;
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();
            String sql = "INSERT INTO ITEMS_RECORD (PRODUCT_NAME, PRODUCT_PRICE, PRODUCT_CAPACITY)VALUES ('"+ item.getName() +"', " + item.getPrice() + ", " + item.getQuantity()+ ");";
            stmt.execute(sql);
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }

        }
    }

}
