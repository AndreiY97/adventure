package sample.Database;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sample.Application.DataTypes.Item;

import java.sql.*;
import java.util.ArrayList;

public class GetItems {

    static final String DB_URL = "jdbc:mysql://localhost:3306/keadb?useSSL=false";

    //  Database credentials
    static final String USER = "root";
    static final String PASS = "root";
    private static ObservableList<Item> items;
    public static ObservableList<Item> getItems(){
        items = FXCollections.observableArrayList();
        Connection conn = null;
        Statement stmt = null;
        Statement stmt2 = null;
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();
            stmt2 = conn.createStatement();
            String sql;
            sql = "select * from ITEMS_RECORD;";
            ResultSet rs = stmt.executeQuery(sql);
            if(rs!=null)
                while (rs.next()){
                    int item_id =  rs.getInt(1);
                    String item_name = rs.getString(2);
                    double item_price = rs.getDouble(3);
                    int item_capacity = rs.getInt(4);
                    Item item = new Item(item_id, item_name, item_price, item_capacity);

                    items.add(item);
            }
            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
            return items;
        }
    }

}
