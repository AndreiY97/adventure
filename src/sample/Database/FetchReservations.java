package sample.Database;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sample.Application.DataTypes.Activities;
import sample.Application.DataTypes.BookingsTable;
import sample.Application.DataTypes.Reservations;
import sample.Application.DataTypes.Reserved_act;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;

public class FetchReservations {

    static final String DB_URL = "jdbc:mysql://localhost:3306/keadb?useSSL=false";

    //  Database credentials
    static final String USER = "root";
    static final String PASS = "root";
    private static ObservableList<BookingsTable> bookingTableItems;
    private static ObservableList<Reservations> bookings;
    public static ObservableList<BookingsTable> filterByDate(LocalDate date){
        bookingTableItems = FXCollections.observableArrayList();
        ArrayList<Reserved_act> activities = new ArrayList<>();
        Connection conn = null;
        Statement stmt = null;
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();
            String sql;
            sql = "SELECT NAME, AGE, ACT_NAME, TIME, RES_DATE, NUMBER_OF_PEOPLE, CUS_TYPE, PHONE, PAYMENT, RES_ID FROM reserved_act b JOIN customers f ON b.CUS_ID = f.CUS_ID AND RES_DATE = '" + date + "' JOIN activities c ON b.ACT_ID = c.ACT_ID ORDER BY RES_DATE";

            ResultSet rs = stmt.executeQuery(sql);
            if(rs!=null)
                while(rs.next()){
                    BookingsTable b = new BookingsTable();
                    b.setCustomerNameDB(rs.getString(1));
                    b.setAgeDB(rs.getString(2));
                    b.setActivityNameDB(rs.getString(3));
                    b.setTimeDB(rs.getString(4));
                    b.setDateDB(rs.getString(5));
                    b.setNumberOfPeople(rs.getString(6));
                    b.setCustomerTypeDB(rs.getString(7));
                    b.setPhoneDB(rs.getString(8));
                    b.setPaymentDB(rs.getString(9).trim() + " dkk");
                    b.setBookingId(rs.getInt(10));

                    bookingTableItems.add(b);
                }
            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {

            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
            return bookingTableItems;
        }
    }

    public static ArrayList<Reserved_act> getByDate(Date date){
        ArrayList<Reserved_act> activities = new ArrayList<>();
        Connection conn = null;
        Statement stmt = null;
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();
            String sql;
            sql = "select * from RESERVED_ACT where RESERVED_ACT.RES_DATE = '" + (new java.sql.Date(date.getTime())) + "'inner JOIN CUSTOMERS on RESERVED_ACT.CUS_ID = CUSTOMERS.CUS_ID inner JOIN ACTIVITIES on RESERVED_ACT.ACT_ID = ACTIVITIES.ACT_ID;";
            ResultSet rs = stmt.executeQuery(sql);
                while(rs.next()){
                    activities.add(new Reserved_act(rs.getInt("RES_ID"), rs.getInt("CUS_ID"), rs.getInt("ACT_ID"),rs.getDouble("PAYMENT"),rs.getInt("NUMBER_OF_PEOPLE"),rs.getInt("AGE_OF_YOUNGEST"), rs.getDate("RES_DATE"), rs.getInt("TIME")));
                }
                rs.close();
                stmt.close();
                conn.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {

            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
            return activities;
        }
    }
    public static ObservableList<Reservations> getBookings(){
        bookings = FXCollections.observableArrayList();
        Connection conn = null;
        Statement stmt = null;
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();
            String sql;
            sql = "SELECT RES_ID, CUS_ID, ACT_ID FROM reserved_act";
            ResultSet rs = stmt.executeQuery(sql);
            /*while(rs.next()){
                activities.add(new Reserved_act(rs.getInt("RES_ID"), rs.getInt("CUS_ID"), rs.getInt("ACT_ID"),rs.getDouble("PAYMENT"),rs.getInt("NUMBER_OF_PEOPLE"),rs.getInt("AGE_OF_YOUNGEST"), rs.getDate("RES_DATE"), rs.getInt("TIME")));
            }*/
            if(rs!=null)
                while(rs.next()){
                    Reservations rsv = new Reservations();
                    rsv.setRes_id(rs.getInt(1));
                    rsv.setCus_id(rs.getInt(2));
                    rsv.setAct_id(rs.getInt(3));
                    bookings.add(rsv);
                }
            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
            return bookings;
        }

    }
    //method to remove a booking
    public static void deleteBooking(Reservations booking){
        Connection conn = null;
        Statement stmt = null;
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();
            String sql, sql1;
            sql="DELETE FROM reserved_act WHERE RES_ID = " + booking.getRes_id() + ";";
            sql1 = "DELETE FROM customers WHERE CUS_ID = " + booking.getCus_id() + ";";
            stmt.executeUpdate(sql1);

            stmt.executeUpdate(sql);



            stmt.close();

            conn.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }

        }
    }
    public static ObservableList<BookingsTable> getBookingTableItems(){
        bookingTableItems = FXCollections.observableArrayList();
        Connection conn = null;
        Statement stmt = null;
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();
            String sql;
            sql = "SELECT NAME, AGE, ACT_NAME, TIME, RES_DATE, NUMBER_OF_PEOPLE, CUS_TYPE, PHONE, PAYMENT, RES_ID FROM reserved_act b JOIN customers f ON b.CUS_ID = f.CUS_ID JOIN activities c ON b.ACT_ID = c.ACT_ID ORDER BY RES_DATE";
            ResultSet rs = stmt.executeQuery(sql);
            /*while(rs.next()){
                activities.add(new Reserved_act(rs.getInt("RES_ID"), rs.getInt("CUS_ID"), rs.getInt("ACT_ID"),rs.getDouble("PAYMENT"),rs.getInt("NUMBER_OF_PEOPLE"),rs.getInt("AGE_OF_YOUNGEST"), rs.getDate("RES_DATE"), rs.getInt("TIME")));
            }*/
            if(rs!=null)
                while(rs.next()){
                    BookingsTable b = new BookingsTable();
                    b.setCustomerNameDB(rs.getString(1));
                    b.setAgeDB(rs.getString(2));
                    b.setActivityNameDB(rs.getString(3));
                    b.setTimeDB(rs.getString(4));
                    b.setDateDB(rs.getString(5));
                    b.setNumberOfPeople(rs.getString(6));
                    b.setCustomerTypeDB(rs.getString(7));
                    b.setPhoneDB(rs.getString(8));
                    b.setPaymentDB(rs.getString(9).trim() + " dkk");
                    b.setBookingId(rs.getInt(10));

                    bookingTableItems.add(b);
                }
            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
            return bookingTableItems;
        }

    }

}
