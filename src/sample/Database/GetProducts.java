package sample.Database;

import sample.Application.DataTypes.Item;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by andreiungureanu on 07/10/2016.
 */
public class GetProducts {

    static final String DB_URL = "jdbc:mysql://localhost:3306/keadb?useSSL=false";

    //  Database credentials
    static final String USER = "root";
    static final String PASS = "root";

    public static ArrayList<String> getProducts(){
        ArrayList<String> products = new ArrayList<>();
        Connection conn = null;
        Statement stmt = null;

        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();
            String sql;
            sql = "select ITEM_TABLE from ITEMS_RECORD;";
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()){
                products.add(rs.getString(1));
            }
            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
            return products;
        }
    }

}
