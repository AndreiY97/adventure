package sample.Database;

import sample.Application.DataTypes.Customer;
import sample.Application.DataTypes.Reserved_act;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by andreiungureanu on 04/10/2016.
 */
public class UpdateReservations {

    static final String DB_URL = "jdbc:mysql://localhost:3306/keadb?useSSL=false";

    //  Database credentials
    static final String USER = "root";
    static final String PASS = "root";


    public static void update(ArrayList<Reserved_act> reserved_acts)

    {
        Connection conn = null;
        Statement stmt = null;

        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();
            String sql;
            sql = " ";
            for (Reserved_act r: reserved_acts){
                if(r.changed){
                    sql="UPDATE RESERVED_ACT SET CUS_ID = " + r.getCus_id() + ",ACT_ID=" + r.getAct_id() + ",PAYMENT=" + r.getPayment() + ",NUMBER_OF_PEOPLE=" + r.getNumber_of_people() + ",AGE_OF_YOUNGEST=" + r.getAge_of_youngest() + ",RES_DATE='" + (new java.sql.Date(r.getRes_date().getTime()) + "',TIME=" + r.getTime() + " WHERE RES_ID = " + r.getRes_id() + ";");
                }
                stmt.execute(sql);
            }
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }

        }

    }
}
